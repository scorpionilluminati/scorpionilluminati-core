# Overview
Scorpion Illuminati is a free open source rhythm game written in Motorola 68000 Assembly language. Scorpion Illuminati is licensed under the Artistic license.

![alt text](http://i.imgur.com/s7A2PrT.gif "Development Screenshot")

## Compiling
You will need to download

| Software | Link |
| ------ | ------ |
| Dosbox | [(https://www.dosbox.com/download.php?main=1](https://www.dosbox.com/download.php?main=1) |
| snasm68k assembler | [(http://segaretro.org/images/3/33/SNASM68K.7z](http://segaretro.org/images/3/33/SNASM68K.7z) |

Run Dosbox and mount the main folder the one that contains this readme
```
mount <drive> <path to main folder>
```

change to the newly mounted drive
```
<drive>:
```

CD to the src folder and inside it are 3 batch files debug.bat, emu.bat and final.bat.
```
cd src
```

To assemble for debugging:
```
debug.bat
```

To assemble for running on an emulator or flash cart:
```
emu.bat
```

To assemble for homemade carts:
```
final.bat
```

## Development
Want to contribute? Great!

Scorpion Illuminati uses Git + Visual Studio Code for fast developing.

Open your favorite Terminal and run these commands.

### Clone the repository
```
git clone https://bitbucket.org/scorpionilluminati/scorpionilluminati-core.git
```

### Install the Motorola 68K Assembly Language Support extension
Launch VS Code Quick Open (Ctrl+P), paste the following command, and press enter.
```
ext install m68k
```

## Note
The framework folder contains the [Big Evil Engine](http://bigevilcorporation.co.uk/) Sega Genesis Framework. It is not complete and work is being working on actively.

## License
This game is licensed Open Source Artistic License with the hopes it will be useful to all.