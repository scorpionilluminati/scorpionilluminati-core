@echo off
rem clrear the screen
cls

rem path to assembler file
set asfile=asm\dos\snasm68k.exe

rem assembler flags
rem /j set include search directory
rem /p pure binary
rem /k enable if assembler directives
set flags=/j \src\ /p /k

rem path to assembler
set aspath=..\..\asm\dos\snasm68k.exe

rem path to source file
set srcfile=\src\source.asm

rem path to binary file
set binfile=\bin\debug\rom.bin

rem path to symbol file
set sympath=\bin\debug\rom

rem path to listing file
set lstpath=\bin\debug\rom

rem path to log file
set logfile=\logs\debug.log

rem full symbol file path
set symfile=\bin\debug\rom.sym

rem full path to list file
set lstfile=\bin\debug\rom.lst

rem **************************************************
rem do not edit below this line
rem **************************************************

rem save the old path
SET oldpath=%path%

rem set the path to the working folder, otherwise compilation will fail
path c:\;%path%

rem go to root folder
c:

rem if the assembler couldn't be found display an error
if NOT exist %aspath% goto LABLASNOTFOUND

rem if the source file couldn't be found display an error
if NOT exist %srcfile% goto LABLSRCNOTFOUND

rem delete old assembled binary if it exists
if exist %binpath% del %binpath%

rem if it couldn't then give permission error
if exist %binpath% goto LABLACCESSBIN

rem delete old symbol file if it exists
if exist %symfile% del %symfile%

rem if it couldn't then give permission error
if exist %symfile% goto LABLACCESSSYM

rem delete old listing file if it exists
if exist %lstfile% del %lstfile%

rem if it couldn't then give permission error
if exist %lstfile% goto LABLACCESSLST

rem delete old listing file if it exists
if exist %logfile% del %logfile%

rem if it couldn't then give permission error
if exist %logfile% goto LABLACCESSLOG

rem assemble the source code
%asfile% %flags% %srcfile%,%binfile%,%sympath%,%lstpath% > %logfile%

rem restore the old path
path %oldpath%

rem if an error occured display an error message
if errorlevel 1 goto LABLASMERROR

echo **************************************************
echo Assembly was successful.
echo **************************************************
goto LABLEXIT

rem **************************************************
rem path to assembler error
rem **************************************************
:LABLASNOTFOUND
echo Could not locate the assembler, please make sure
echo the path to the assembler is correct and is installed
echo correctly.
goto LABLEXIT

rem **************************************************
rem path to source file error
rem **************************************************
:LABLSRCNOTFOUND
echo Could not locate the source file, please make sure
echo the path to the source file is correct.
goto LABLEXIT

rem **************************************************
rem binary write permissions error
rem **************************************************
:LABLACCESSBIN
echo Could not delete old binary, access denied.
goto LABLEXIT

rem **************************************************
rem symbol write permissions error
rem **************************************************
:LABLACCESSSYM
echo Could not delete old symbol file, access denied.
goto LABLEXIT

rem **************************************************
rem listing write permissions error
rem **************************************************
:LABLACCESSLST
echo Could not delete old listing file, access denied.
goto LABLEXIT

rem **************************************************
rem log write permissions error
rem **************************************************
:LABLACCESSLOG
echo Could not delete old log file, access denied
goto LABLEXIT

rem **************************************************
rem Assembly error
rem **************************************************
:LABLASMERROR
echo The assembler has reported errors. Please check
echo %logfile% for details
goto LABLEXIT

:LABLEXIT
pause