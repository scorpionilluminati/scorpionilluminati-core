@echo off
rem delete old assembled binary if it exists
@echo on
if exist ..\..\bin\debug\rom.bin del ..\..\bin\debug\rom.bin

@echo off
rem /j set include search directory
rem /p pure binary
rem /k enable if assembler directives
@echo on
..\..\asm\win32\asm68k.exe /j ..\..\src\* /p /k ..\..\src\source.asm,..\..\bin\debug\rom.bin,..\..\bin\debug\rom,..\..\bin\debug\rom > ..\..\logs\debug.log

pause