@echo off
rem delete old assembled binary if it exists
@echo on
if exist ..\..\bin\emu\rom.bin del ..\..\bin\emu\rom.bin

@echo off
rem /j set include search directory
rem /p pure binary
rem /k enable if assembler directives
rem /o enable optimisations
rem ow+ optimise absolute long addressing
@echo on
..\..\asm\win32\asm68k.exe /j ..\..\src\* /p /k /o op+,os+,ow+,oz+,oaq+,osq+,omq+ ..\..\src\source.asm,..\..\bin\emu\rom.bin,..\..\bin\emu\rom,..\..\bin\emu\rom > ..\..\logs\emu.log

pause