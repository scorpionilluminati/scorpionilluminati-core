@echo off
rem delete old assembled binary if it exists
@echo on
if exist ..\..\bin\final\rom.bin del ..\..\bin\final\rom.bin

@echo off
rem /j set include search directory
rem /p pure binary
rem /k enable if assembler directives
@echo on
..\..\asm\win32\asm68k.exe /j ..\..\src\* /p /k ..\..\src\source.asm,..\..\bin\final\rom.bin,..\..\bin\final\rom,..\..\bin\final\rom > ..\..\logs\final.log

pause