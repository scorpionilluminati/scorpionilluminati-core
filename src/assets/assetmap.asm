;==============================================================
;   Scorpion Illuminati
;==============================================================
;   SEGA Genesis (c) SegaDev 2014
;==============================================================

; ************************************
; Art asset VRAM mapping
; ************************************
PixelFontVRAM:            equ 0x0000
TitleScreenTilesVRAM:     equ PixelFontVRAM+PixelFontSizeB
SonglistScreenTilesVRAM:  equ TitleScreenTilesVRAM+TitleScreenTilesSizeB
GameTilesVRAM:            equ SonglistScreenTilesVRAM+SonglistScreenTilesSizeB
StatsScreenTilesVRAM:     equ GameTilesVRAM+GameTilesSizeB
CrashHandlerTilesVRAM:    equ StatsScreenTilesVRAM+StatsScreenTilesSizeB
SelectionArrowVRAM:       equ CrashHandlerTilesVRAM+CrashHandlerTilesSizeB
RockIndicatorVRAM:        equ SelectionArrowVRAM+SelectionArrowSizeB
GreenNoteVRAM:            equ RockIndicatorVRAM+RockIndicatorSizeB
RedNoteVRAM:              equ GreenNoteVRAM+GreenNoteSizeB

; ************************************
; Include all art assets
; ************************************
      include assets\fonts\pixelfnt.asm
      include assets\sprites\selarrow.asm
      include assets\sprites\grnnote.asm
      include assets\sprites\rednote.asm
      include assets\sprites\rockind.asm
      include assets\tiles\title.asm
      include assets\tiles\songlist.asm
      include assets\tiles\game.asm
      include assets\tiles\stats.asm
      include assets\tiles\crashand.asm
      include assets\maps\title.asm
      include assets\maps\songlist.asm
      include assets\maps\game.asm
      include assets\maps\stats.asm
      include assets\maps\blank.asm

; ************************************
; Include all music data
; ************************************
      include assets\songs\inst.asm
      include assets\songs\songlist.asm

; ************************************
; Include all beatmap data
; ************************************
      include assets\beatmaps\comrdeoj\twgythng.asm
      include assets\beatmaps\linkuebr\ttlce.asm

; ************************************
; Include all cue data
; ************************************
      include assets\cues\dfltcue.asm

; ************************************
; Include all palettes
; ************************************
      include assets\palettes\palettes.asm

; ************************************
; Include all text strings
; ************************************
      include assets\strings\strings.asm
      include assets\strings\authors.asm
      include assets\strings\songname.asm
      include assets\strings\debug.asm
      even