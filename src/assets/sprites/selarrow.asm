;==============================================================
;   Scorpion Illuminati
;==============================================================
;   SEGA Genesis (c) SegaDev
;==============================================================

SelectionArrow:

      dc.l	0x00033000                                                         ;    XX   
      dc.l	0x00033300                                                         ;    X X  
      dc.l	0x33333330                                                         ; XXXX  X 
      dc.l	0x33333333                                                         ; X      X
      dc.l	0x33333333                                                         ; X      X
      dc.l	0x33333330                                                         ; XXXX  X 
      dc.l	0x00033300                                                         ;    X X  
      dc.l	0x00033000                                                         ;    XX   

SelectionArrowEnd                                                              ; Sprite end address
SelectionArrowSizeB: equ (SelectionArrowEnd-SelectionArrow)                    ; Sprite size in bytes
SelectionArrowSizeW: equ (SelectionArrowSizeB/2)                               ; Sprite size in words
SelectionArrowSizeL: equ (SelectionArrowSizeB/4)                               ; Sprite size in longs
SelectionArrowSizeT: equ (SelectionArrowSizeB/32)                              ; Sprite size in tiles
SelectionArrowTileID: equ (SelectionArrowVRAM/32)                              ; ID of first tile
SelectionArrowDimensionBits: equ (%00000000)                                   ; Sprite dimensions (1x1)

SelectionArrowDimensions: equ 0x0101                                           ; 1x1 subsprites
SelectionArrowSizeS: equ 1                                                     ; Sprite size in 4x4 subsprites
SelectionArrowWidth: equ 0x08                                                  ; cursor width in pixels
SelectionArrowHeight: equ 0x08                                                 ; cursor height in pixels

; Subsprite dimensions
SelectionArrowSubSpriteDimensions:
      dc.w 0x0101                                                              ; Top-left     (1x1)
      even