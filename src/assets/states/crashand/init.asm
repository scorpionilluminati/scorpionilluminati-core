CrashHandler_Init:
      move.l sp, (backup_sp)                                                   ; save sp state
      move.l a0, (register_backup_a0)                                          ; save a0 register state
      move.l a1, (register_backup_a1)                                          ; save a1 register state
      move.l a2, (register_backup_a2)                                          ; save a2 register state
      move.l a3, (register_backup_a3)                                          ; save a3 register state
      move.l a4, (register_backup_a4)                                          ; save a4 register state
      move.l a5, (register_backup_a5)                                          ; save a5 register state
      move.l a6, (register_backup_a6)                                          ; save a6 register state
      move.l d0, (register_backup_d0)                                          ; save d0 register state
      move.l d1, (register_backup_d1)                                          ; save d1 register state
      move.l d2, (register_backup_d2)                                          ; save d2 register state
      move.l d3, (register_backup_d3)                                          ; save d3 register state
      move.l d4, (register_backup_d4)                                          ; save d4 register state
      move.l d5, (register_backup_d5)                                          ; save d5 register state
      move.l d6, (register_backup_d6)                                          ; save d6 register state
      move.l d7, (register_backup_d7)                                          ; save d7 register state

      moveq #0x1, d0
      jsr WaitFrames ; Wait a frame, to collect new joypad presses

      ; ************************************
      ; Load blank map
      ; ************************************
      lea BlankMap, a0                                                         ; Map data in a0
      move.w #BlankMapSizeW, d0                                                ; Size (words) in d0
      moveq #0x0, d1                                                           ; Y offset in d1
      move.w #CrashHandlerTilesTileID, d2                                      ; First tile ID in d2
      moveq #0x0, d3                                                           ; Palette ID in d3
      jsr LoadMapPlaneB                                                        ; Jump to subroutine

      ; ************************************
      ;  Draw The Error String
      ; ************************************
      lea ZeroDivideString, a0                                                 ; String address
      move.l #PixelFontTileID, d0                                              ; First tile id
      move.w #0x0C04, d1                                                       ; XY (7, 8)
      moveq #0x0, d2                                                           ; Palette 0
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The a0 register String
      ; ************************************
      lea RegisterA0String, a0                                                 ; String address
      move.l #PixelFontTileID, d0                                              ; First tile id
      move.w #0x0508, d1                                                       ; XY (7, 8)
      moveq #0x0, d2                                                           ; Palette 0
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The a1 register String
      ; ************************************
      lea RegisterA1String, a0                                                 ; String address
      move.w #0x050A, d1                                                       ; XY (7, 10)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The a2 register String
      ; ************************************
      lea RegisterA2String, a0                                                 ; String address
      move.w #0x050C, d1                                                       ; XY (7, 12)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The a3 register String
      ; ************************************
      lea RegisterA3String, a0                                                 ; String address
      move.w #0x050E, d1                                                       ; XY (7, 14)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The a4 register String
      ; ************************************
      lea RegisterA4String, a0                                                 ; String address
      move.w #0x0510, d1                                                       ; XY (7, 16)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The a5 register String
      ; ************************************
      lea RegisterA5String, a0                                                 ; String address
      move.w #0x0512, d1                                                       ; XY (7, 18)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The a6 register String
      ; ************************************
      lea RegisterA6String, a0                                                 ; String address
      move.w #0x0514, d1                                                       ; XY (7, 20)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The sp register String
      ; ************************************
      lea StackPointerString, a0                                               ; String address
      move.w #0x0518, d1                                                       ; XY (7, 20)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The d0 register String
      ; ************************************
      lea RegisterD0String, a0                                                 ; String address
      move.w #0x1508, d1                                                       ; XY (20, 8)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The d1 register String
      ; ************************************
      lea RegisterD1String, a0                                                 ; String address
      move.w #0x150A, d1                                                       ; XY (20, 10)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The d2 register String
      ; ************************************
      lea RegisterD2String, a0                                                 ; String address
      move.w #0x150C, d1                                                       ; XY (20, 12)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The d3 register String
      ; ************************************
      lea RegisterD3String, a0                                                 ; String address
      move.w #0x150E, d1                                                       ; XY (20, 14)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The d4 register String
      ; ************************************
      lea RegisterD4String, a0                                                 ; String address
      move.w #0x1510, d1                                                       ; XY (20, 16)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The d5 register String
      ; ************************************
      lea RegisterD5String, a0                                                 ; String address
      move.w #0x1512, d1                                                       ; XY (20, 18)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The d6 register String
      ; ************************************
      lea RegisterD6String, a0                                                 ; String address
      move.w #0x1514, d1                                                       ; XY (20, 16)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The d7 register String
      ; ************************************
      lea RegisterD7String, a0                                                 ; String address
      move.w #0x1516, d1                                                       ; XY (20, 18)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The pc register String
      ; ************************************
      lea ProgramCounterString, a0                                             ; String address
      move.w #0x1518, d1                                                       ; XY (7, 20)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      lea -$4(sp), sp                                                          ; load effective address of stack pointer
      move.l sp, a0                                                            ; allocate temporary buffer on stack

      ; draw the a0 counter
      move.l sp, a0                                                            ; String to a0
      move.w (register_backup_a0), d0                                          ; Integer to d0
      jsr    ItoA_Hex_l                                                        ; Integer to ASCII (word)

      move.l sp, a0                                                            ; String to a0
      move.l #PixelFontTileID, d0                                              ; Font to d0
      move.l #0x0908, d1                                                       ; Position to d1
      moveq #0x0, d2                                                           ; Palette to d2
      jsr DrawTextPlaneA                                                       ; Draw text

      ; draw the a1 counter
      move.l sp, a0                                                            ; String to a0
      move.w (register_backup_a1), d0                                          ; Integer to d0
      jsr    ItoA_Hex_l                                                        ; Integer to ASCII (word)

      move.l sp, a0                                                            ; String to a0
      move.l #PixelFontTileID, d0                                              ; Font to d0
      move.l #0x090A, d1                                                       ; Position to d1
      moveq #0x0, d2                                                           ; Palette to d2
      jsr DrawTextPlaneA                                                       ; Draw text

      ; draw the a2 counter
      move.l sp, a0                                                            ; String to a0
      move.w (register_backup_a2), d0                                          ; Integer to d0
      jsr    ItoA_Hex_l                                                        ; Integer to ASCII (word)

      move.l sp, a0                                                            ; String to a0
      move.l #PixelFontTileID, d0                                              ; Font to d0
      move.l #0x090C, d1                                                       ; Position to d1
      moveq #0x0, d2                                                           ; Palette to d2
      jsr DrawTextPlaneA                                                       ; Draw text

      ; draw the a3 counter
      move.l sp, a0                                                            ; String to a0
      move.w (register_backup_a3), d0                                          ; Integer to d0
      jsr    ItoA_Hex_l                                                        ; Integer to ASCII (word)

      move.l sp, a0                                                            ; String to a0
      move.l #PixelFontTileID, d0                                              ; Font to d0
      move.l #0x090E, d1                                                       ; Position to d1
      moveq #0x0, d2                                                           ; Palette to d2
      jsr DrawTextPlaneA                                                       ; Draw text

      ; draw the a4 counter
      move.l sp, a0                                                            ; String to a0
      move.w (register_backup_a4), d0                                          ; Integer to d0
      jsr    ItoA_Hex_l                                                        ; Integer to ASCII (word)

      move.l sp, a0                                                            ; String to a0
      move.l #PixelFontTileID, d0                                              ; Font to d0
      move.l #0x0910, d1                                                       ; Position to d1
      moveq #0x0, d2                                                           ; Palette to d2
      jsr DrawTextPlaneA                                                       ; Draw text

      ; draw the a5 counter
      move.l sp, a0                                                            ; String to a0
      move.w (register_backup_a5), d0                                          ; Integer to d0
      jsr    ItoA_Hex_l                                                        ; Integer to ASCII (word)

      move.l sp, a0                                                            ; String to a0
      move.l #PixelFontTileID, d0                                              ; Font to d0
      move.l #0x0912, d1                                                       ; Position to d1
      moveq #0x0, d2                                                           ; Palette to d2
      jsr DrawTextPlaneA                                                       ; Draw text

      ; draw the a6 counter
      move.l sp, a0                                                            ; String to a0
      move.w (register_backup_a6), d0                                          ; Integer to d0
      jsr    ItoA_Hex_l                                                        ; Integer to ASCII (word)

      move.l sp, a0                                                            ; String to a0
      move.l #PixelFontTileID, d0                                              ; Font to d0
      move.l #0x0914, d1                                                       ; Position to d1
      moveq #0x0, d2                                                           ; Palette to d2
      jsr DrawTextPlaneA                                                       ; Draw text

      ; draw the sp counter
      move.l sp, a0                                                            ; String to a0
      move.w (backup_sp), d0                                                   ; Integer to d0
      jsr    ItoA_Hex_l                                                        ; Integer to ASCII (word)

      move.l sp, a0                                                            ; String to a0
      move.l #PixelFontTileID, d0                                              ; Font to d0
      move.l #0x0918, d1                                                       ; Position to d1
      moveq #0x0, d2                                                           ; Palette to d2
      jsr DrawTextPlaneA                                                       ; Draw text

      ; draw the d0 counter
      move.l sp, a0                                                            ; String to a0
      move.w (register_backup_d0), d0                                          ; Integer to d0
      jsr    ItoA_Hex_l                                                        ; Integer to ASCII (word)

      move.l sp, a0                                                            ; String to a0
      move.l #PixelFontTileID, d0                                              ; Font to d0
      move.l #0x1908, d1                                                       ; Position to d1
      moveq #0x0, d2                                                           ; Palette to d2
      jsr DrawTextPlaneA                                                       ; Draw text

      ; draw the d1 counter
      move.l sp, a0                                                            ; String to a0
      move.w (register_backup_d1), d0                                          ; Integer to d0
      jsr    ItoA_Hex_l                                                        ; Integer to ASCII (word)

      move.l sp, a0                                                            ; String to a0
      move.l #PixelFontTileID, d0                                              ; Font to d0
      move.l #0x190A, d1                                                       ; Position to d1
      moveq #0x0, d2                                                           ; Palette to d2
      jsr DrawTextPlaneA                                                       ; Draw text

      ; draw the d2 counter
      move.l sp, a0                                                            ; String to a0
      move.w (register_backup_d2), d0                                          ; Integer to d0
      jsr    ItoA_Hex_l                                                        ; Integer to ASCII (word)

      move.l sp, a0                                                            ; String to a0
      move.l #PixelFontTileID, d0                                              ; Font to d0
      move.l #0x190C, d1                                                       ; Position to d1
      moveq #0x0, d2                                                           ; Palette to d2
      jsr DrawTextPlaneA                                                       ; Draw text

      ; draw the d3 counter
      move.l sp, a0                                                            ; String to a0
      move.w (register_backup_d3), d0                                          ; Integer to d0
      jsr    ItoA_Hex_l                                                        ; Integer to ASCII (word)

      move.l sp, a0                                                            ; String to a0
      move.l #PixelFontTileID, d0                                              ; Font to d0
      move.l #0x190E, d1                                                       ; Position to d1
      moveq #0x0, d2                                                           ; Palette to d2
      jsr DrawTextPlaneA                                                       ; Draw text

      ; draw the d4 counter
      move.l sp, a0                                                            ; String to a0
      move.w (register_backup_d4), d0                                          ; Integer to d0
      jsr    ItoA_Hex_l                                                        ; Integer to ASCII (word)

      move.l sp, a0                                                            ; String to a0
      move.l #PixelFontTileID, d0                                              ; Font to d0
      move.l #0x1910, d1                                                       ; Position to d1
      moveq #0x0, d2                                                           ; Palette to d2
      jsr DrawTextPlaneA                                                       ; Draw text

      ; draw the d5 counter
      move.l sp, a0                                                            ; String to a0
      move.w (register_backup_d5), d0                                          ; Integer to d0
      jsr    ItoA_Hex_l                                                        ; Integer to ASCII (word)

      move.l sp, a0                                                            ; String to a0
      move.l #PixelFontTileID, d0                                              ; Font to d0
      move.l #0x1912, d1                                                       ; Position to d1
      moveq #0x0, d2                                                           ; Palette to d2
      jsr DrawTextPlaneA                                                       ; Draw text

      ; draw the d6 counter
      move.l sp, a0                                                            ; String to a0
      move.w (register_backup_d6), d0                                          ; Integer to d0
      jsr    ItoA_Hex_l                                                        ; Integer to ASCII (word)

      move.l sp, a0                                                            ; String to a0
      move.l #PixelFontTileID, d0                                              ; Font to d0
      move.l #0x1914, d1                                                       ; Position to d1
      moveq #0x0, d2                                                           ; Palette to d2
      jsr DrawTextPlaneA                                                       ; Draw text

      ; draw the d7 counter
      move.l sp, a0                                                            ; String to a0
      move.w (register_backup_d7), d0                                          ; Integer to d0
      jsr    ItoA_Hex_l                                                        ; Integer to ASCII (word)

      move.l sp, a0                                                            ; String to a0
      move.l #PixelFontTileID, d0                                              ; Font to d0
      move.l #0x1916, d1                                                       ; Position to d1
      moveq #0x0, d2                                                           ; Palette to d2
      jsr DrawTextPlaneA                                                       ; Draw text

      ; draw the pc counter
      move.l sp, a0                                                            ; String to a0
      move.w (backup_pc), d0                                                   ; Integer to d0
      jsr    ItoA_Hex_l                                                        ; Integer to ASCII (word)

      move.l sp, a0                                                            ; String to a0
      move.l #PixelFontTileID, d0                                              ; Font to d0
      move.l #0x1918, d1                                                       ; Position to d1
      moveq #0x0, d2                                                           ; Palette to d2
      jsr DrawTextPlaneA                                                       ; Draw text

      lea $4(sp), sp                                                           ; free allocated temporary buffer

      jmp CrashHandler                                                         ; jump to crash handler