GameMode_Init:
      ; ************************************
      ; Load game map
      ; ************************************
      lea GameMap, a0                                                          ; Map data in a0
      move.w #GameMapSizeW, d0                                                 ; Size (words) in d0
      moveq #0x0, d1                                                           ; Y offset in d1
      move.w #GameTilesTileID, d2                                              ; First tile ID in d2
      moveq #0x0, d3                                                           ; Palette ID in d3
      jsr LoadMapPlaneB                                                        ; Jump to subroutine

      ; ************************************
      ;  Draw The Score String
      ; ************************************
      lea ScoreString, a0                                                      ; String address
      move.l #PixelFontTileID, d0                                              ; First tile id
      move.w #0x0301, d1                                                       ; XY (5, 1)
      moveq #0x0, d2                                                           ; Palette 0
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The Combo String
      ; ************************************
      lea ComboString, a0                                                      ; String address
      move.w #0x1C01, d1                                                       ; XY (27, 1)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ; Initalize Everything
      ; ************************************
      move.w #0, (score)                                                       ; initialize score
      move.w #0, (combo)                                                       ; initialize combo
      move.w #1, (multiplier)                                                  ; initialize multiplier
      move.w #1, (scoredelta)                                                  ; initalize score delta
      move.w #1, (tempo)                                                       ; initialize tempo
      move.w #(note_start_position_x), (greennote_position_x)                  ; Set green note's x position
      move.w #(note_start_position_x), (rednote_position_x)                    ; Set red note's x position
      move.w #rockindicator_start_position_x, (rockindicator_position_x)       ; Set rock indicator's x position

      ; load the song data for the 1st song
      ;move.l #0x0, d3
      ;jsr GetSongData

      ; ************************************
      ; Load music
      ; ************************************
      lea ComradeOj_TwangyThing, a0                                         ; song data address in a1
      ;lea LinkueiBR_TotalConcentration, a0                                 ; song data address in a1
      jsr Echo_PlayBGM                                                      ; play the song
      move.w #game_state_game_mode, game_state                              ; set game state to game mode

      ; ************************************
      ; hide the selection arrow from songlist
      ; ************************************
      move.w #offscreen_position_y, (selectionarrow_position_y)                ; move the selection arrow's y position offscreen
      move.w #selectionarrow_id, d0                                            ; selection arrow's sprite id
      move.w (selectionarrow_position_y), d1                                   ; selection arrow's y position
      move.w #SelectionArrowDimensions, d2                                     ; selection arrow's dimensions
      moveq #0x8, d3                                                           ; selection arrrow's width in pixels
      moveq #0x0, d4                                                           ; selection arrow's x flipped
      lea SelectionArrowSubSpriteDimensions, a1                                ; selection arrow's subsprite 
      jsr SetSpritePosY                                                        ; Set selection arrow's y position

      moveq #0x1, d0
      jsr WaitFrames                                                           ; Wait a frame, to collect new joypad presses
      rts