GameMode_Shutdown:
      ; ************************************
      ; Stop Music
      ; ************************************
      jsr Echo_StopBGM                                                         ; stop the music

      ; ************************************
      ; Clear the sprites
      ; ************************************
      move.w #offscreen_position_y, (greennote_position_x)                     ; Set green note's y position
      move.w #offscreen_position_y, (rednote_position_x)                       ; Set red note's y position
      move.w #offscreen_position_x, (rockindicator_position_x)                 ; Set rock indicator's x position

      ; ************************************
      ;  Clear The Score String
      ; ************************************
      lea BlankString, a0                                                      ; String address
      move.l #PixelFontTileID, d0                                              ; First tile id
      move.w #0x0301, d1                                                       ; XY (5, 1)
      moveq #0x0, d2                                                           ; Palette 0
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Clear The Score Counter
      ; ************************************
      lea BlankString, a0                                                      ; String to a0
      move.l #PixelFontTileID, d0                                              ; Font to d0
      move.l #0x0901, d1                                                       ; Position to d1
      moveq #0x0, d2                                                           ; Palette to d2
      jsr DrawTextPlaneA                                                       ; Draw text

      ; ************************************
      ;  Clear The Combo String
      ; ************************************
      lea BlankString, a0                                                      ; String address
      move.w #0x1C01, d1                                                       ; XY (27, 1)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ; Clear The Combo Counter
      ; ************************************
      lea BlankString, a0                                                      ; String to a0
      move.l #PixelFontTileID, d0                                              ; Font to d0
      move.l #0x2201, d1                                                       ; Position to d1
      moveq #0x0, d2                                                           ; Palette to d2
      jsr DrawTextPlaneA                                                       ; Draw text

      ; ************************************
      ; Clear The Multiplier Counter
      ; ************************************
      lea BlankString, a0                                                      ; String to a0
      move.l #0x0419, d1                                                       ; Position to d1
      jsr DrawTextPlaneA                                                       ; Draw text
      move.w #game_state_stats_screen_initalize, game_state                    ; store the game state
      rts