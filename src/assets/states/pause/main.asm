Pause:
      move.w joypadA_press, d0                                                 ; Read pad 1 state, result in d0
      btst #pad_button_start, d0                                               ; Check start button
      beq.s Pause                                                              ; if not then continue paused, otherwise clear the pause string...
      lea BlankString, a0                                                      ; String address
      move.l #PixelFontTileID, d0                                              ; First tile id
      move.w #0x0810, d1                                                       ; XY (08, 16)
      moveq #0x0, d2                                                           ; Palette 0
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine
      move.w #game_state_game_mode_pause_shutdown, game_state                  ; store the game state
      ; Since we skip game_state 0x2, we need to gather new inputs here instead
      moveq #0x1, d0
      jmp WaitFrames                                                           ; Wait a frame, to collect new joypad presses
      rts