SonglistScreen_Init:
      ; ************************************
      ; Load songlist screen map
      ; ************************************
      lea SonglistScreenMap, a0                                                ; Map data in a0
      move.w #SonglistScreenMapSizeW, d0                                       ; Size (words) in d0
      moveq #0x0, d1                                                           ; Y offset in d1
      move.w #SonglistScreenTilesTileID, d2                                    ; First tile ID in d2
      moveq #0x0, d3                                                           ; Palette ID in d3
      jsr LoadMapPlaneB                                                        ; Jump to subroutine

      ; ************************************
      ;  Draw The Songlist String
      ; ************************************
      lea SonglistString, a0                                                   ; String address
      move.l #PixelFontTileID, d0                                              ; First tile id
      move.w #0x0F02, d1                                                       ; XY (15, 2)
      moveq #0x0, d2                                                           ; Palette 0
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; load the meta data for the 1st song
      ;move.l #0x0, d3
      ;jsr GetSongMetaData

      ; ************************************
      ;  Draw The 1st song String
      ; ************************************
      lea StringAuthorComradeOj, a0                                            ; String address
      move.w #0x0705, d1                                                       ; XY (7, 5)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The 1st song creator String
      ; ************************************
      lea StringSongTwangyThing, a0                                            ; String address
      move.w #0x0706, d1                                                       ; XY (7, 6)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; load the meta data for the 2nd song
;     move.l #0x1, d3
;     jsr GetSongMetaData

      ; ************************************
      ;  Draw The 2nd song String
      ; ************************************
;     lea (a5), a0                                                             ; String address
;     move.w #0x0709, d1                                                       ; XY (7, 9)
;     jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The 2nd song creator String
      ; ************************************
;     lea (a4), a0                                                             ; String address
;     move.w #0x070A, d1                                                       ; XY (7, 10)
;     jsr DrawTextPlaneA                                                       ; Call draw text subroutine

     bsr ClearSprites                                                          ; Clear Sprite List

     ; add the selection table to the sprite list
      move.w #selectionarrow_start_position_y, d0                              ; Y coord (+ 128)
      move.b #SelectionArrowDimensionBits, d1                                  ; Width (bits 0-1) and height (bits 2-3) in tiles
      move.b 0x00, d2                                                          ; H/V flipping (bits 3/4), palette index (bits 5-6), priority (bit 7)
      move.b #SelectionArrowTileID, d3                                         ; Index of first tile
      move.w #selectionarrow_start_position_x, d4                              ; x coord (+ 128)
      lea (spritelist_table), a0                                               ; load address of sprite list table into a0
      bsr AddSprite                                                            ; add the sprite to the list

     ; *************************************
     ; Set selection arrows initial y position
     ; *************************************
      move.w #selectionarrow_start_position_y, (selectionarrow_position_y)     ; set selection arrow's y position first

     ; *************************************
     ; set default song id
     ; *************************************
     move.w #0, (song_id)                                                      ; set default song to first one

      moveq #0x1, d0
      jsr WaitFrames                                                           ; Wait a frame, to collect new joypad presses

      move.w  #game_state_songlist_screen, game_state                          ; store the game state
      rts