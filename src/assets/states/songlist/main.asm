SonglistScreen:
      ; *************************************
      ; draw the selection arrow
      ; *************************************
;     move.w #selectionarrow_id, d0                                            ; selection arrow's sprite id
;     move.w #selectionarrow_start_position_x, d1                              ; selection arrow's x position
;     move.w #SelectionArrowDimensions, d2                                     ; selection arrow's dimensions
;     moveq #0x8, d3                                                           ; selection arrrow's width in pixels
;     moveq #0x0, d4                                                           ; selection arrow's x flipped
;     lea SelectionArrowSubSpriteDimensions, a1                                ; selection arrow's subsprite 
;     jsr SetSpritePosX                                                        ; Set selection arrow's x position

;     move.w #selectionarrow_id, d0                                            ; selection arrow's sprite id
;     move.w (selectionarrow_position_y), d1                                   ; selection arrow's y position
;     jsr SetSpritePosY                                                        ; Set selection arrow's y position

      bsr UpdateSprites                                                        ; update the sprites list

      move.w joypadA_press, d0                                                 ; Read pad 1 state, result in d0
;     move.w (selectionarrow_position_y), d1                                   ; selection arrows y position in d1
;     move.w #selectionarrow_y_velocity, d2                                    ; selection arrows y velocity in d2
;     move.w #selectionarrow_boundry_top, d3                                   ; selection arrows top boundry in d3
;     move.w #selectionarrow_boundry_bottom, d4                                ; selection arrows bottom boundry in d4
;     move.w (song_id), d5
;     btst #pad_button_up, d0                                                  ; did the user press up on the dpad?
;     beq.s SonglistNotDPadUp                                                  ; if not the branch and don't move the selection arrow up
;     cmp.w d1, d3                                                             ; has the selection arrow reached the top boundry
;     beq.s SelectionArrowHitTopBoundry                                        ; if it has then don't do anything
;     sub d2, d1                                                               ; subtract the selection arrows velocity to it's position 
;SelectionArrowHitTopBoundry:
;SonglistNotDPadUp:
;     btst #pad_button_down, d0                                                ; did the user press down on the dpad?
;     beq.s SonglistNotDPadDown                                                ; if not the branch and don't move the selection arrow down
;     cmp.w d1, d4                                                             ; has the selection arrow reached the bottom boundry
;     beq.s SelectionArrowHitBottomBoundry                                     ; if it has then don't do anything
;     add d2, d1                                                               ; add the selection arrows velocity to it's position                                                                    ; temporary placeholder for when dpad is pressed down
;SelectionArrowHitBottomBoundry:
;SonglistNotDPadDown:
;     move.w d1, (selectionarrow_position_y)                                   ; store the selectoion arrows new y position
      btst #pad_button_start, d0                                               ; Check start button
      beq.s SonglistScreen                                                     ; if not then continue to show the player's stats
;     move.w d5, (song_id)                                                     ; store the song id
      move.w #game_state_songlist_screen_shutdown, game_state                  ; otherwise clean up the songlist state
      moveq #0x1, d0
      jmp WaitFrames                                                           ; Wait a frame, to collect new joypad presses
      rts