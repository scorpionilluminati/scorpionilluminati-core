SonglistScreen_Shutdown:
      ; ************************************
      ;  Clear The Songlist String
      ; ************************************
      lea BlankString, a0                                                   ; String address
      move.l #PixelFontTileID, d0                                              ; First tile id
      move.w #0x0F02, d1                                                       ; XY (15, 2)
      moveq #0x0, d2                                                           ; Palette 0
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Clear The 1st song String
      ; ************************************
      lea BlankString, a0                                                      ; String address
      move.w #0x0705, d1                                                       ; XY (7, 5)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Clear The 1st song creator String
      ; ************************************
      lea BlankString, a0                                                      ; String address
      move.w #0x0706, d1                                                       ; XY (7, 6)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Clear The 2nd song String
      ; ************************************
;     lea BlankString, a0                                                      ; String address
;     move.w #0x0709, d1                                                       ; XY (7, 9)
;     jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The 2nd song creator String
      ; ************************************
;     lea BlankString, a0                                                      ; String address
;     move.w #0x070A, d1                                                       ; XY (7, 10)
;     jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Clear The Press Start String
      ; ************************************
      lea BlankString, a0                                                      ; String address
      move.w #0x0F1A, d1                                                       ; XY (15, 26)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine
      move.w #game_state_game_mode_initalize, game_state                       ; store the game state

      if EmulateDivideByZeroCrash=1
      divu.w  #0, d0                                                           ; disable for release
      endc
      rts