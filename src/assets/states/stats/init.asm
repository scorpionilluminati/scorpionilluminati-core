StatsScreen_Init:
      ; ************************************
      ; Temporary Placeholders
      ; ************************************
      move.w #42, d0
      jsr ItoBCD_Int_w
      move.w d2, (hits)
      move.w #0, (misses)
      move.w #0, (errors)
      move.w #138, d0
      jsr ItoBCD_Int_w
      move.w d2, (score)
      move.w #42, d0
      jsr ItoBCD_Int_w
      move.w d2, (top_streak)
      move.w #100, d0
      jsr ItoBCD_Int_w
      move.w d2, (Completion)

      ; start of vblank
      jsr WaitVBlankStart

      ; ************************************
      ; Set The Sprites  Position Offscreen
      ; ************************************
      ; Set green note's position
      move.w #GreenNoteDimensions, d2                                          ; green fret's dimensions
      moveq #0x8, d3                                                           ; green fret's width in pixels
      moveq #0x0, d4                                                           ; green fret's x flipped
      lea GreenNoteSubSpriteDimensions, a1                                     ; green fret's subsprite 

      move.w #greennote_id, d0                                                 ; green's sprite id
      move.w (greennote_position_x), d1                                        ; green's x position
      jsr SetSpritePosX                                                        ; Set green note's x position

      move.w #rednote_id, d0                                                   ; red note's sprite id
      move.w (rednote_position_x), d1                                          ; red note's x position
      jsr SetSpritePosX                                                        ; Set red note's x position

      move.w #rockindicator_id, d0                                             ; rock indicator's sprite id
      move.w (rockindicator_position_x), d1                                    ; rock indicator's x position
      jsr SetSpritePosX                                                        ; Set rock indicator's y position

      ; ************************************
      ; Load stats screen map
      ; ************************************
      lea StatsScreenMap, a0                                                   ; Map data in a0
      move.w #TitleScreenMapSizeW, d0                                          ; Size (words) in d0
      moveq #0x0, d1                                                           ; Y offset in d1
      move.w #StatsScreenTilesTileID, d2                                       ; First tile ID in d2
      moveq #0x0, d3                                                           ; Palette ID in d3
      jsr LoadMapPlaneB                                                        ; Jump to subroutine

      ; ************************************
      ;  Draw The Statistics String
      ; ************************************
      lea StatisticsString, a0                                                 ; String address
      move.l #PixelFontTileID, d0                                              ; First tile id
      move.w #0x0F05, d1                                                       ; XY (15, 5)
      moveq #0x0, d2                                                           ; Palette 0
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The Hits String
      ; ************************************
      lea HitsString, a0                                                      ; String address
      move.l #PixelFontTileID, d0                                              ; First tile id
      move.w #0x0607, d1                                                       ; XY (6, 7)
      moveq #0x0, d2                                                           ; Palette 0
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The Misses String
      ; ************************************
      lea MissesString, a0                                                      ; String address
      move.l #PixelFontTileID, d0                                              ; First tile id
      move.w #0x0609, d1                                                       ; XY (6, 9)
      moveq #0x0, d2                                                           ; Palette 0
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The Errors String
      ; ************************************
      lea ErrorsString, a0                                                     ; String address
      move.l #PixelFontTileID, d0                                              ; First tile id
      move.w #0x060B, d1                                                       ; XY (6, 11)
      moveq #0x0, d2                                                           ; Palette 0
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The Score String
      ; ************************************
      lea ScoreString, a0                                                      ; String address
      move.l #PixelFontTileID, d0                                              ; First tile id
      move.w #0x060E, d1                                                       ; XY (6, 14)
      moveq #0x0, d2                                                           ; Palette 0
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The Top Streak String
      ; ************************************
      lea TopStreakString, a0                                                  ; String address
      move.l #PixelFontTileID, d0                                              ; First tile id
      move.w #0x0611, d1                                                       ; XY (6, 17)
      moveq #0x0, d2                                                           ; Palette 0
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Draw The Completion String
      ; ************************************
      lea CompletionString, a0                                                 ; String address
      move.l #PixelFontTileID, d0                                              ; First tile id
      move.w #0x0614, d1                                                       ; XY (6, 20)
      moveq #0x0, d2                                                           ; Palette 0
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine
      moveq #0x1, d0

      ; ************************************
      ; start of stack allocation
      ; ************************************
      lea -$8(sp), sp                                                          ; load effective address of stack pointer
      move.l sp, a0                                                            ; allocate temporary buffer on stack

      ; *************************************
      ; convert the hits counter to int
      ; *************************************
      move.l sp, a0                                                            ; String to a0
      move.w (hits), d0                                                        ; Integer to d0
      jsr    ItoA_Int_w                                                        ; Integer to ASCII (word)

      ; *************************************
      ; draw the hits counter
      ; *************************************
      move.l sp, a0                                                            ; String to a0
      move.l #PixelFontTileID, d0                                              ; Font to d0
      move.l #0x1C07, d1                                                       ; Position to d1
      moveq #0x0, d2                                                           ; Palette to d2
      jsr DrawTextPlaneA                                                       ; Draw text

      ; *************************************
      ; convert the misses counter to int
      ; *************************************
      move.l sp, a0                                                            ; String to a0
      move.w (misses), d0                                                      ; Integer to d0
      jsr    ItoA_Int_w                                                        ; Integer to ASCII (word)

      ; *************************************
      ; draw the misses counter
      ; *************************************
      move.l sp, a0                                                            ; String to a0
      move.l #PixelFontTileID, d0                                              ; Font to d0
      move.l #0x1C09, d1                                                       ; Position to d1
      moveq #0x0, d2                                                           ; Palette to d2
      jsr DrawTextPlaneA                                                       ; Draw text

      ; *************************************
      ; convert the errors counter to int
      ; *************************************
      move.l sp, a0                                                            ; String to a0
      move.w (errors), d0                                                      ; Integer to d0
      jsr    ItoA_Int_w                                                        ; Integer to ASCII (word)

      ; *************************************
      ; draw the errors counter
      ; *************************************
      move.l sp, a0                                                            ; String to a0
      move.l #PixelFontTileID, d0                                              ; Font to d0
      move.l #0x1C0B, d1                                                       ; Position to d1
      moveq #0x0, d2                                                           ; Palette to d2
      jsr DrawTextPlaneA                                                       ; Draw text

      ; *************************************
      ; convert the score counter to int
      ; *************************************
      move.l sp, a0                                                            ; String to a0
      move.w (score), d0                                                       ; Integer to d0
      jsr    ItoA_Int_w                                                        ; Integer to ASCII (word)

      ; *************************************
      ; draw the errors counter
      ; *************************************
      move.l sp, a0                                                            ; String to a0
      move.l #PixelFontTileID, d0                                              ; Font to d0
      move.l #0x1C0E, d1                                                       ; Position to d1
      moveq #0x0, d2                                                           ; Palette to d2
      jsr DrawTextPlaneA                                                       ; Draw text

      ; *************************************
      ; convert the top streak counter to int
      ; *************************************
      move.l sp, a0                                                            ; String to a0
      move.w (top_streak), d0                                                  ; Integer to d0
      jsr    ItoA_Int_w                                                        ; Integer to ASCII (word)

      ; *************************************
      ; draw the top streak counter
      ; *************************************
      move.l sp, a0                                                            ; String to a0
      move.l #PixelFontTileID, d0                                              ; Font to d0
      move.l #0x1C11, d1                                                       ; Position to d1
      moveq #0x0, d2                                                           ; Palette to d2
      jsr DrawTextPlaneA                                                       ; Draw text

      ; *************************************
      ; convert the completion counter to int
      ; *************************************
       move.l sp, a0                                                            ; String to a0
       move.w (completion), d0                                                  ; Integer to d0
       jsr    ItoA_Int_w                                                        ; Integer to ASCII (word)

      ; *************************************
      ; draw the completion counter
      ; *************************************
      move.l sp, a0                                                            ; String to a0
      move.l #PixelFontTileID, d0                                              ; Font to d0
      move.l #0x1C14, d1                                                       ; Position to d1
      moveq #0x0, d2                                                           ; Palette to d2
      jsr DrawTextPlaneA                                                       ; Draw text

      lea $8(sp), sp                                                           ; free allocated temporary buffer

      ; end of vblank
      jsr WaitVBlankEnd

      moveq #0x1, d0
      jsr WaitFrames                                                           ; Wait a frame, to collect new joypad presses

      move.w  #game_state_stats_screen, game_state                             ; store the game state
      rts