StatsScreen:
      move.w joypadA_press, d0                                                 ; Read pad 1 state, result in d0
      btst #pad_button_start, d0                                               ; Check start button
      beq.s StatsScreen                                                        ; if not then continue to show the player's stats
      move.w #game_state_stats_screen_shutdown, game_state                     ; store the game state
      moveq #0x1, d0
      jmp WaitFrames                                                           ; Wait a frame, to collect new joypad presses
      rts