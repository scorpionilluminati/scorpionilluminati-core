StatsScreen_Shutdown:
      ; ************************************
      ;  Clear The Statistics String
      ; ************************************
      lea BlankString, a0                                                      ; String address
      move.l #PixelFontTileID, d0                                              ; First tile id
      move.w #0x0F05, d1                                                       ; XY (15, 5)
      moveq #0x0, d2                                                           ; Palette 0
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; ************************************
      ;  Clear The Hits String
      ; ************************************
      lea BlankString, a0                                                      ; String address
      move.w #0x0607, d1                                                       ; XY (6, 7)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; *************************************
      ; Clear the hits counter
      ; *************************************
      lea BlankString, a0                                                      ; String to a0
      move.l #0x1C07, d1                                                       ; Position to d1
      jsr DrawTextPlaneA                                                       ; Draw text

      ; ************************************
      ;  Clear The Misses String
      ; ************************************
      lea BlankString, a0                                                      ; String address
      move.w #0x0609, d1                                                       ; XY (6, 9)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; *************************************
      ; Clear the misses counter
      ; *************************************
      lea BlankString, a0                                                      ; String to a0
      move.l #0x1C09, d1                                                       ; Position to d1
      jsr DrawTextPlaneA                                                       ; Draw text

      ; ************************************
      ;  Clear The Errors String
      ; ************************************
      lea BlankString, a0                                                      ; String address
      move.w #0x060B, d1                                                       ; XY (6, 11)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; *************************************
      ; Clear the errors counter
      ; *************************************
      lea BlankString, a0                                                      ; String to a0
      move.l #0x1C0B, d1                                                       ; Position to d1
      jsr DrawTextPlaneA                                                       ; Draw text

      ; ************************************
      ;  Clear The Score String
      ; ************************************
      lea BlankString, a0                                                      ; String address
      move.w #0x060E, d1                                                       ; XY (6, 14)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; *************************************
      ; Clear the errors counter
      ; *************************************
      lea BlankString, a0                                                      ; String to a0
      move.l #0x1C0E, d1                                                       ; Position to d1
      jsr DrawTextPlaneA                                                       ; Draw text

      ; ************************************
      ;  Clear The Top Streak String
      ; ************************************
      lea BlankString, a0                                                      ; String address
      move.w #0x0611, d1                                                       ; XY (6, 17)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; *************************************
      ; Clear the top streak counter
      ; *************************************
      lea BlankString, a0                                                      ; String to a0
      move.l #0x1C11, d1                                                       ; Position to d1
      jsr DrawTextPlaneA                                                       ; Draw text

      ; ************************************
      ;  Draw The Completion String
      ; ************************************
      lea BlankString, a0                                                      ; String address
      move.w #0x0614, d1                                                       ; XY (6, 20)
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine

      ; *************************************
      ; Clear the completion counter
      ; *************************************
      lea BlankString, a0                                                      ; String to a0
      move.l #0x1C14, d1                                                       ; Position to d1
      jsr DrawTextPlaneA                                                       ; Draw text
      move.w #game_state_title_screen_initalize, game_state                    ; store the game state
      rts