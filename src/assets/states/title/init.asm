TitleScreen_Init:
      moveq #0x1, d0
      jsr WaitFrames ; Wait a frame, to collect new joypad presses

      ; ************************************
      ; Load title screen map
      ; ************************************
      lea TitleScreenMap, a0                                                   ; Map data in a0
      move.w #TitleScreenMapSizeW, d0                                          ; Size (words) in d0
      moveq #0x0, d1                                                           ; Y offset in d1
      move.w #TitleScreenTilesTileID, d2                                       ; First tile ID in d2
      moveq #0x0, d3                                                           ; Palette ID in d3
      jsr LoadMapPlaneB                                                        ; Jump to subroutine
      ; ************************************
      ;  Draw The Press Start String
      ; ************************************
      lea PressStartString, a0                                                 ; String address
      move.l #PixelFontTileID, d0                                              ; First tile id
      move.w #0x0F1A, d1                                                       ; XY (15, 5)
      moveq #0x0, d2                                                           ; Palette 0
      jsr DrawTextPlaneA                                                       ; Call draw text subroutine
      move.w  #game_state_title_screen, game_state                             ; store the game state
      rts