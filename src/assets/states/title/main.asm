TitleScreen:
      move.w joypadA_press, d0                                                 ; Read pad 1 state, result in d0
      btst #pad_button_start, d0                                               ; Check start button
      beq.s TitleScreen                                                        ; if not then do the title screen
      move.w #game_state_title_screen_shutdown, game_state                     ; store the game state
      rts