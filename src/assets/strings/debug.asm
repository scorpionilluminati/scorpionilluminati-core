RegisterA0String:
      dc.b "A0: ",0

RegisterA1String:
      dc.b "A1: ",0

RegisterA2String:
      dc.b "A2: ",0

RegisterA3String:
      dc.b "A3: ",0

RegisterA4String:
      dc.b "A4: ",0

RegisterA5String:
      dc.b "A5: ",0

RegisterA6String:
      dc.b "A6: ",0

StackPointerString:
      dc.b "SP: ",0

ProgramCounterString:
      dc.b "PC: ",0

RegisterD0String:
      dc.b "D0: ",0

RegisterD1String:
      dc.b "D1: ",0

RegisterD2String:
      dc.b "D2: ",0

RegisterD3String:
      dc.b "D3: ",0

RegisterD4String:
      dc.b "D4: ",0

RegisterD5String:
      dc.b "D5: ",0

RegisterD6String:
      dc.b "D6: ",0

RegisterD7String:
      dc.b "D7: ",0

BusErrorString:
      dc.b "BUS ERROR",0

AddressErrorString:
      dc.b "ADDRESS ERROR",0

IllegalInstructionString:
      dc.b "ILLEGAL INSTRUCTION",0

ZeroDivideString:
      dc.b "ZERO DIVIDE",0

CHKInstructionString:
      dc.b "CHK INSTRUCTION",0

TRAPVInstructionString:
      dc.b "TRAPV INSTRUCTION",0

PrivilegeViolationString:
      dc.b "PRIVILEGE VIOLATION",0

TraceString:
      dc.b "TRACE",0

LineAEmulatorString:
      dc.b "LINE-A EMULATOR",0

LineFEmulatorString:
      dc.b "LINE-F EMULATOR",0

Trap00ExceptionString:
      dc.b "TRAP 00 EXCEPTION",0

Trap01ExceptionString:
      dc.b "TRAP 01 EXCEPTION",0

Trap02ExceptionString:
      dc.b "TRAP 02 EXCEPTION",0

Trap03ExceptionString:
      dc.b "TRAP 03 EXCEPTION",0

Trap04ExceptionString:
      dc.b "TRAP 04 EXCEPTION",0

Trap05ExceptionString:
      dc.b "TRAP 05 EXCEPTION",0

Trap06ExceptionString:
      dc.b "TRAP 06 EXCEPTION",0

Trap07ExceptionString:
      dc.b "TRAP 07 EXCEPTION",0

Trap08ExceptionString:
      dc.b "TRAP 08 EXCEPTION",0

Trap09ExceptionString:
      dc.b "TRAP 09 EXCEPTION",0

Trap10ExceptionString:
      dc.b "TRAP 10 EXCEPTION",0

Trap11ExceptionString:
      dc.b "TRAP 11 EXCEPTION",0

Trap12ExceptionString:
      dc.b "TRAP 12 EXCEPTION",0

Trap13ExceptionString:
      dc.b "TRAP 13 EXCEPTION",0

Trap14ExceptionString:
      dc.b "TRAP 14 EXCEPTION",0

Trap15ExceptionString:
      dc.b "TRAP 15 EXCEPTION",0