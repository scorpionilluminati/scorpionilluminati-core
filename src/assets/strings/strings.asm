;==============================================================
;   Scorpion Illuminati
;==============================================================
;   SEGA Genesis (c) SegaDev 2014
;==============================================================
BlankString:
      dc.b "                    ",0

PressStartString:
      dc.b "PRESS START",0

SonglistString:
      dc.b "SONGLIST",0

ComboString:
      dc.b "COMBO",0

CompletionString:
      dc.b "COMPLETION",0

ErrorsString:
      dc.b "ERRORS",0

HitsString:
      dc.b "HITS",0

MissesString:
      dc.b "MISSES",0

MultiplierString:
      dc.b "  X",0

PauseString:
      dc.b "PAUSE",0

PercentString:
      dc.b "o\o",0

ScoreString:
      dc.b "SCORE",0

StatisticsString:
      dc.b "STATISTICS",0

TopStreakString:
      dc.b "TOP STREAK",0
      even