;==============================================================
;   Scorpion Illuminati
;==============================================================
;   SEGA Genesis (c) SegaDev 2014
;==============================================================

CrashHandlerTiles:
      dc.l	$00000000                                                          ; 
      dc.l	$00000000                                                          ;
      dc.l	$00000000                                                          ;
      dc.l	$00000000                                                          ;
      dc.l	$00000000                                                          ;
      dc.l	$00000000                                                          ;
      dc.l	$00000000                                                          ;
      dc.l	$00000000                                                          ; 

CrashHandlerTilesEnd                                                           ; Tiles end address
CrashHandlerTilesSizeB: equ (CrashHandlerTilesEnd-CrashHandlerTiles)           ; Tiles size in bytes
CrashHandlerTilesSizeW: equ (CrashHandlerTilesSizeB/SizeWord)                  ; Tiles size in words
CrashHandlerTilesSizeL: equ (CrashHandlerTilesSizeB/SizeLong)                  ; Tiles size in longs
CrashHandlerTilesSizeT: equ (CrashHandlerTilesSizeB/SizeTile)                   ; Tiles size in tiles
CrashHandlerTilesTileID: equ (CrashHandlerTilesVRAM/SizeTile)                  ; ID of first tile