;==============================================================
;   Scorpion Illuminati
;==============================================================
;   SEGA Genesis (c) SegaDev 2014
;==============================================================

GameTiles:
      dc.l	$00000000                                                          ; 
      dc.l	$00000000                                                          ;
      dc.l	$00000000                                                          ;
      dc.l	$00000000                                                          ;
      dc.l	$00000000                                                          ;
      dc.l	$00000000                                                          ;
      dc.l	$00000000                                                          ;
      dc.l	$00000000                                                          ; 

      dc.l  $00000000                                                          ;         
      dc.l  $00000000                                                          ;         
      dc.l  $55555555                                                          ;   XXXX  
      dc.l  $55555555                                                          ;   XXXX  
      dc.l  $55555555                                                          ;   XXXX  
      dc.l  $55555555                                                          ;   XXXX  
      dc.l  $00000000                                                          ;         
      dc.l  $00000000                                                          ;         

      dc.l  $00000000                                                          ;         
      dc.l  $00000000                                                          ;         
      dc.l  $BBBBBBBB                                                          ;   XXXX  
      dc.l  $BBBBBBBB                                                          ;   XXXX  
      dc.l  $BBBBBBBB                                                          ;   XXXX  
      dc.l  $BBBBBBBB                                                          ;   XXXX  
      dc.l  $00000000                                                          ;         
      dc.l  $00000000                                                          ;         

      dc.l  $000BB000                                                          ;    XX   
      dc.l  $000BB000                                                          ;    XX   
      dc.l  $000BB000                                                          ;    XX   
      dc.l  $000BB000                                                          ;    XX   
      dc.l  $000BB000                                                          ;    XX   
      dc.l  $000BB000                                                          ;    XX   
      dc.l  $000BB000                                                          ;    XX   
      dc.l  $000BB000                                                          ;    XX   

      dc.l	$00AAAA00                                                          ;   XXXX  
      dc.l	$0AA00AA0                                                          ;  XX  XX 
      dc.l	$AA0BB0AA                                                          ; XX XX XX
      dc.l	$A0BBBB0A                                                          ; X XXXX X
      dc.l	$A0BBBB0A                                                          ; X XXXX X
      dc.l	$AA0BB0AA                                                          ; XX XX XX
      dc.l	$0AA00AA0                                                          ;  XX  XX 
      dc.l	$00AAAA00                                                          ;   XXXX  

      dc.l  $55555555                                                          ; XXXXXXXX
      dc.l  $22222222                                                          ; XXXXXXXX
      dc.l  $22222222                                                          ; XXXXXXXX
      dc.l  $22222222                                                          ; XXXXXXXX
      dc.l  $22222222                                                          ; XXXXXXXX
      dc.l  $22222222                                                          ; XXXXXXXX
      dc.l  $22222222                                                          ; XXXXXXXX
      dc.l  $55555555                                                          ; XXXXXXXX
 
      dc.l  $55555555                                                          ; XXXXXXXX
      dc.l  $66666666                                                          ; XXXXXXXX
      dc.l  $66666666                                                          ; XXXXXXXX
      dc.l  $66666666                                                          ; XXXXXXXX
      dc.l  $66666666                                                          ; XXXXXXXX
      dc.l  $66666666                                                          ; XXXXXXXX
      dc.l  $66666666                                                          ; XXXXXXXX
      dc.l  $55555555                                                          ; XXXXXXXX
 
      dc.l  $55555555                                                          ; XXXXXXXX
      dc.l  $11111111                                                          ; XXXXXXXX
      dc.l  $11111111                                                          ; XXXXXXXX
      dc.l  $11111111                                                          ; XXXXXXXX
      dc.l  $11111111                                                          ; XXXXXXXX
      dc.l  $11111111                                                          ; XXXXXXXX
      dc.l  $11111111                                                          ; XXXXXXXX
      dc.l  $55555555                                                          ; XXXXXXXX

GameTilesEnd                                                                   ; Tiles end address
GameTilesSizeB: equ (GameTilesEnd-GameTiles)                                   ; Tiles size in bytes
GameTilesSizeW: equ (GameTilesSizeB/SizeWord)                                  ; Tiles size in words
GameTilesSizeL: equ (GameTilesSizeB/SizeLong)                                  ; Tiles size in longs
GameTilesSizeT: equ (GameTilesSizeB/SizeTile)                                  ; Tiles size in tiles
GameTilesTileID: equ (GameTilesVRAM/SizeTile)                                  ; ID of first tile