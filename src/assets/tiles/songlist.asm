;==============================================================
;   Scorpion Illuminati
;==============================================================
;   SEGA Genesis (c) SegaDev 2014
;==============================================================

SonglistScreenTiles:
      dc.l	$00000000                                                          ; 
      dc.l	$00000000                                                          ;
      dc.l	$00000000                                                          ;
      dc.l	$00000000                                                          ;
      dc.l	$00000000                                                          ;
      dc.l	$00000000                                                          ;
      dc.l	$00000000                                                          ;
      dc.l	$00000000                                                          ; 

      dc.l  $55555555                                                          ; XXXXXXXX
      dc.l  $55555555                                                          ; XXXXXXXX
      dc.l  $55555555                                                          ; XXXXXXXX
      dc.l  $55555555                                                          ; XXXXXXXX
      dc.l  $55555555                                                          ; XXXXXXXX
      dc.l  $55555555                                                          ; XXXXXXXX
      dc.l  $55555555                                                          ; XXXXXXXX
      dc.l  $55555555                                                          ; XXXXXXXX

      dc.l  $54545454                                                          ; X X X X 
      dc.l  $45454545                                                          ;  X X X X
      dc.l  $54545454                                                          ; X X X X 
      dc.l  $45454545                                                          ;  X X X X
      dc.l  $54545454                                                          ; X X X X 
      dc.l  $45454545                                                          ;  X X X X
      dc.l  $54545454                                                          ; X X X X 
      dc.l  $45454545                                                          ;  X X X X

      dc.l  $55444455                                                          ;   XXXX  
      dc.l  $54444445                                                          ;  XXXXXX 
      dc.l  $44444444                                                          ; XXXXXXXX
      dc.l  $44444444                                                          ; XXXXXXXX
      dc.l  $44444444                                                          ; XXXXXXXX
      dc.l  $44444444                                                          ; XXXXXXXX
      dc.l  $54444445                                                          ;  XXXXXX 
      dc.l  $55444455                                                          ;   XXXX  

SonglistScreenTilesEnd                                                         ; Tiles end address
SonglistScreenTilesSizeB: equ (SonglistScreenTilesEnd-SonglistScreenTiles)     ; Tiles size in bytes
SonglistScreenTilesSizeW: equ (SonglistScreenTilesSizeB/SizeWord)              ; Tiles size in words
SonglistScreenTilesSizeL: equ (SonglistScreenTilesSizeB/SizeLong)              ; Tiles size in longs
SonglistScreenTilesSizeT: equ (SonglistScreenTilesSizeB/SizeTile)              ; Tiles size in tiles
SonglistScreenTilesTileID: equ (SonglistScreenTilesVRAM/SizeTile)              ; ID of first tile