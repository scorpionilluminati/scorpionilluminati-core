;==============================================================
;   Scorpion Illuminati
;==============================================================
;   SEGA Genesis (c) SegaDev 2014
;==============================================================

StatsScreenTiles:
      dc.l	$00000000                                                          ; 
      dc.l	$00000000                                                          ;
      dc.l	$00000000                                                          ;
      dc.l	$00000000                                                          ;
      dc.l	$00000000                                                          ;
      dc.l	$00000000                                                          ;
      dc.l	$00000000                                                          ;
      dc.l	$00000000                                                          ; 

      dc.l  $11111111                                                          ; 
      dc.l  $11111511                                                          ; XXXXXXXX
      dc.l  $15555551                                                          ; X      X
      dc.l  $11511111                                                          ; X      X
      dc.l  $15555551                                                          ; X      X
      dc.l  $11115111                                                          ; X      X
      dc.l  $15555551                                                          ; XXXXXXXX
      dc.l  $11111111                                                          ;         

StatsScreenTilesEnd                                                            ; Tiles end address
StatsScreenTilesSizeB: equ (StatsScreenTilesEnd-StatsScreenTiles)              ; Tiles size in bytes
StatsScreenTilesSizeW: equ (StatsScreenTilesSizeB/SizeWord)                    ; Tiles size in words
StatsScreenTilesSizeL: equ (StatsScreenTilesSizeB/SizeLong)                    ; Tiles size in longs
StatsScreenTilesSizeT: equ (StatsScreenTilesSizeB/SizeTile)                    ; Tiles size in tiles
StatsScreenTilesTileID: equ (StatsScreenTilesVRAM/SizeTile)                    ; ID of first tile