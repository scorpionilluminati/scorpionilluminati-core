;==============================================================
;   BIG EVIL CORPORATION .co.uk
;==============================================================
;   SEGA Genesis Framework (c) Matt Phillips 2014
;==============================================================
;   interpts.asm - Interrupts and exceptions
;==============================================================

HBlankInterrupt:
   addi.l #0x1, hblank_counter    ; Increment hinterrupt counter
   rte

VBlankInterrupt:
   ; Backup registers
   movem.l d0-a7,-(sp)

   ; Cache Joypad inputs
   jsr ReadPadA
   jsr ReadPadB

   addi.l #0x1, vblank_counter    ; Increment vinterrupt counter
   TRAP #0 ; Sync with debugger - NOT FOR RELEASE

   ; Restore registers
   movem.l (sp)+,d0-a7
   rte

Exception:
   if EnableDebug=1
   TRAP #0 ; Sync with debugger - NOT FOR RELEASE
   stop #$2700 ; Halt CPU
   TRAP #0 ; Sync with debugger - NOT FOR RELEASE
   jmp Exception
   endc
   rte

BusErrorException:
   move.w 2(sp), (backup_pc)                                                   ; save pc state
   move.l #BusErrorString, (error_code)                                        ; store error code in ram
   jmp CrashHandler_Init                                                       ; jump to crash handler
   stop #$2700                                                                 ; Halt CPU
   rte

AddressErrorException:
   move.w 8(sp), (backup_pc)                                                   ; save pc state
   move.l #AddressErrorString, (error_code)                                    ; store error code in ram
   jmp CrashHandler_Init                                                       ; jump to crash handler
   stop #$2700                                                                 ; Halt CPU
   rte

IllegalInstructionException:
   move.w 2(sp), (backup_pc)                                                   ; save pc state
   move.l #IllegalInstructionString, (error_code)                              ; store error code in ram
   jmp CrashHandler_Init                                                       ; jump to crash handler
   stop #$2700                                                                 ; Halt CPU
   rte

ZeroDivideException:
   move.w 2(sp), (backup_pc)                                                   ; save pc state
   move.l #ZeroDivideString, (error_code)                                      ; store error code in ram
   jmp CrashHandler_Init                                                       ; jump to crash handler
   stop #$2700                                                                 ; Halt CPU
   rte

CHKInstructionException:
   move.w 2(sp), (backup_pc)                                                   ; save pc state
   move.l #CHKInstructionString, (error_code)                                  ; store error code in ram
   jmp CrashHandler_Init                                                       ; jump to crash handler
   stop #$2700                                                                 ; Halt CPU
   rte

TRAPVInstructionException:
   move.w 2(sp), (backup_pc)                                                   ; save pc state
   move.l #TRAPVInstructionString, (error_code)                                ; store error code in ram
   jmp CrashHandler_Init                                                       ; jump to crash handler
   stop #$2700                                                                 ; Halt CPU
   rte

PrivilegeViolationException:
   move.w 2(sp), (backup_pc)                                                   ; save pc state
   move.l #PrivilegeViolationString, (error_code)                              ; store error code in ram
   jmp CrashHandler_Init                                                       ; jump to crash handler
   stop #$2700                                                                 ; Halt CPU
   rte

TRACEException:
   move.w 2(sp), (backup_pc)                                                   ; save pc state
   move.l #TRACEString, (error_code)                                           ; store error code in ram
   jmp CrashHandler_Init                                                       ; jump to crash handler
   stop #$2700                                                                 ; Halt CPU
   rte

LineAEmulator:
   move.w 2(sp), (backup_pc)                                                   ; save pc state
   move.l #LineAEmulatorString, (error_code)                                   ; store error code in ram
   jmp CrashHandler_Init                                                       ; jump to crash handler
   stop #$2700                                                                 ; Halt CPU
   rte

LineFEmulator:
   move.w 2(sp), (backup_pc)                                                   ; save pc state
   move.l #LineFEmulatorString, (error_code)                                   ; store error code in ram
   jmp CrashHandler_Init                                                       ; jump to crash handler
   stop #$2700                                                                 ; Halt CPU
   rte

TRAP00Exception:
   move.w 2(sp), (backup_pc)                                                   ; save pc state
   move.l #TRAP00ExceptionString, (error_code)                                 ; store error code in ram
   jmp CrashHandler_Init                                                       ; jump to crash handler
   stop #$2700                                                                 ; Halt CPU
   rte

TRAP01Exception:
   move.w 2(sp), (backup_pc)                                                   ; save pc state
   move.l #TRAP01ExceptionString, (error_code)                                 ; store error code in ram
   jmp CrashHandler_Init                                                       ; jump to crash handler
   stop #$2700                                                                 ; Halt CPU
   rte

TRAP02Exception:
   move.w 2(sp), (backup_pc)                                                   ; save pc state
   move.l #TRAP02ExceptionString, (error_code)                                 ; store error code in ram
   jmp CrashHandler_Init                                                       ; jump to crash handler
   stop #$2700                                                                 ; Halt CPU
   rte

TRAP03Exception:
   move.w 2(sp), (backup_pc)                                                   ; save pc state
   move.l #TRAP03ExceptionString, (error_code)                                 ; store error code in ram
   jmp CrashHandler_Init                                                       ; jump to crash handler
   stop #$2700                                                                 ; Halt CPU
   rte

TRAP04Exception:
   move.w 2(sp), (backup_pc)                                                   ; save pc state
   move.l #TRAP04ExceptionString, (error_code)                                 ; store error code in ram
   jmp CrashHandler_Init                                                       ; jump to crash handler
   stop #$2700                                                                 ; Halt CPU
   rte

TRAP05Exception:
   move.w 2(sp), (backup_pc)                                                   ; save pc state
   move.l #TRAP05ExceptionString, (error_code)                                 ; store error code in ram
   jmp CrashHandler_Init                                                       ; jump to crash handler
   stop #$2700                                                                 ; Halt CPU
   rte

TRAP06Exception:
   move.w 2(sp), (backup_pc)                                                   ; save pc state
   move.l #TRAP06ExceptionString, (error_code)                                 ; store error code in ram
   jmp CrashHandler_Init                                                       ; jump to crash handler
   stop #$2700                                                                 ; Halt CPU
   rte

TRAP07Exception:
   move.w 2(sp), (backup_pc)                                                   ; save pc state
   move.l #TRAP07ExceptionString, (error_code)                                 ; store error code in ram
   jmp CrashHandler_Init                                                       ; jump to crash handler
   stop #$2700                                                                 ; Halt CPU
   rte

TRAP08Exception:
   move.w 2(sp), (backup_pc)                                                   ; save pc state
   move.l #TRAP08ExceptionString, (error_code)                                 ; store error code in ram
   jmp CrashHandler_Init                                                       ; jump to crash handler
   stop #$2700                                                                 ; Halt CPU
   rte

TRAP09Exception:
   move.w 2(sp), (backup_pc)                                                   ; save pc state
   move.l #TRAP09ExceptionString, (error_code)                                 ; store error code in ram
   jmp CrashHandler_Init                                                       ; jump to crash handler
   stop #$2700                                                                 ; Halt CPU
   rte
TRAP10Exception:
   move.w 2(sp), (backup_pc)                                                   ; save pc state
   move.l #TRAP10ExceptionString, (error_code)                                 ; store error code in ram
   jmp CrashHandler_Init                                                       ; jump to crash handler
   stop #$2700                                                                 ; Halt CPU
   rte

TRAP11Exception:
   move.w 2(sp), (backup_pc)                                                   ; save pc state
   move.l #TRAP11ExceptionString, (error_code)                                 ; store error code in ram
   jmp CrashHandler_Init                                                       ; jump to crash handler
   stop #$2700                                                                 ; Halt CPU
   rte

TRAP12Exception:
   move.w 2(sp), (backup_pc)                                                   ; save pc state
   move.l #TRAP12ExceptionString, (error_code)                                 ; store error code in ram
   jmp CrashHandler_Init                                                       ; jump to crash handler
   stop #$2700                                                                 ; Halt CPU
   rte

TRAP13Exception:
   move.w 2(sp), (backup_pc)                                                   ; save pc state
   move.l #TRAP13ExceptionString, (error_code)                                 ; store error code in ram
   jmp CrashHandler_Init                                                       ; jump to crash handler
   stop #$2700                                                                 ; Halt CPU
   rte

TRAP14Exception:
   move.w 2(sp), (backup_pc)                                                   ; save pc state
   move.l #TRAP14ExceptionString, (error_code)                                 ; store error code in ram
   jmp CrashHandler_Init                                                       ; jump to crash handler
   stop #$2700                                                                 ; Halt CPU
   rte

TRAP15Exception:
   move.w 2(sp), (backup_pc)                                                   ; save pc state
   move.l #TRAP15ExceptionString, (error_code)                                 ; store error code in ram
   jmp CrashHandler_Init                                                       ; jump to crash handler
   stop #$2700                                                                 ; Halt CPU
   rte

NullInterrupt:
   rte