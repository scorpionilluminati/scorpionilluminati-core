; ******************************************
; push register to stack (byte)
; PUSHL register to push
; ******************************************
PUSHB: macro reg
    move.b \reg, -(sp)
    endm

; ******************************************
; pop register from the stack (byte)
; POPL register to pop in reverse order
; ******************************************
POPB: macro reg
    move.b (sp)+, \reg
    endm

; ******************************************
; push register to stack (word)
; PUSHL register to push
; ******************************************
PUSHW: macro reg
    move.w \reg, -(sp)
    endm

; ******************************************
; pop register from the stack (word)
; POPL register to pop in reverse order
; ******************************************
POPW: macro reg
    move.w (sp)+, \reg
    endm

; ******************************************
; push register to stack (longword)
; PUSHL register to push
; ******************************************
PUSHL: macro reg
    move.l \reg, -(sp)
    endm

; ******************************************
; pop register from the stack (longword)
; POPL register to pop in reverse order
; ******************************************
POPL: macro reg
    move.l (sp)+, \reg
    endm

; ******************************************
; Forces alignment on an even address in ram
; rseven
; ******************************************
RSEVEN: macro
    if __RS&1
    rs.b 1
    endc
    endm