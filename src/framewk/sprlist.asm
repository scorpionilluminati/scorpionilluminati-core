; *************************************************************
; ClearSprite
; Clears the sprite counter
; *************************************************************
ClearSprites:
    move.b #0, (sprite_count)                                                  ; set sprite count to 0
    rts

; *************************************************************
; AddSprite
; Adds a sprite to the sprite table and increments counter
; d0 - sprites y position
; d1 - width and height tile bits
; d2 - special (flipping, palette index, priority)
; d3 - tile id
; d4 - sprites x position
; *************************************************************
AddSprite:
    clr.w d5                                                                   ; clear register d5
    move.b (sprite_count), d5                                                  ; sprint count in d5
    cmp.b #79, d5                                                              ; are there too many sprites in the table already?
    bgt.s SkipAddSprite                                                        ; if not branch
    clr.w d6                                                                   ; clear register d6
    move.b d5, d6                                                              ; d6 is used to calculate the sprite table offset
    lsl.w #$03, d6                                                             ; calculate sprite table offset (sprite count * sprite size)
    lea (spritelist_table), a0                                                 ; sprite table in a0
    add.w d6, a0                                                               ; increment address by offset
    move.w d0, (a0)+                                                           ; move sprites y position into table
    move.b d1, (a0)+                                                           ; move sprites demensions into table
    addq.l #1, d5                                                              ; increment d5(sprite_count) by 1
    move.b d5, (a0)+                                                           ; index of next sprite is d5(sprite_count +1) 
    move.b d2, (a0)+                                                           ; save sprites special bits
    move.b d3, (a0)+                                                           ; save sprites tile id in table
    move.w d4, (a0)+                                                           ; save sprites x position in table
    addq #1, (sprite_count)                                                    ; increment sprite counter by 1
SkipAddSprite:
    rts

; *************************************************************
; Updates and draws the sprite table
; *************************************************************
UpdateSprites:
    move.b (sprite_count), d0                                                  ; load sprite counter into d0
	and.l #0x000000FF, d0
	mulu.w #SizeSpriteDesc, d0                                                 ; Offset into sprite table
	swap d0                                                                    ; To upper word
	or.l #vdp_write_sprite_table, d0                                           ; Add to write address
	move.l d0, vdp_control                                                     ; Set read address
    lea (spritelist_table), a0                                                 ; sprite table in a0
	move.l (a0)+, vdp_data                                                     ; 8 bytes of data
	move.l (a0)+, vdp_data
    rts

; *************************************************************
; Removes a sprite from the sprite table
; d0 - which sprite to remove
; *************************************************************
RemoveSprite:
    clr.w d1                                                                   ; clear register d0
    move.b (sprite_count), d1                                                  ; sprint count in d0
    cmp.b #0, d1                                                               ; are there any sprites in the table?
    beq.s SkipRemoveSprite                                                     ; if not branch
    lsl.w #$03, d0                                                             ; calculate sprite table offset (sprite count * sprite size)
    lea (spritelist_table), a0                                                 ; sprite table in a0
    add.w d0, a0                                                               ; increment address by offset
    move.w #0, (a0)+                                                           ; move sprites y position into table
    move.b #0, (a0)+                                                           ; move sprites demensions into table
    move.b #0, (a0)+                                                           ; index of next sprite is d5(sprite_count +1) 
    move.b #0, (a0)+                                                           ; save sprites special bits
    move.b #0, (a0)+                                                           ; save sprites tile id in table
    move.w #0, (a0)+                                                           ; save sprites x position in table
    subq #8, a0                                                                ; go back to start of deleted sprite data for sorting subroutine
                                                                               ; to start sorting the index from the correct position
    subq #1, (sprite_count)                                                    ; increment sprite counter by 1
    bsr.s SortSpriteTableByAddress                                             ; sort the sprite list
SkipRemoveSprite:
    rts

; *************************************************************
; Sorts the sprite table by sprite id
; d0 - sprite id to start sorting from
; *************************************************************
SortSpriteTableByIndex:
    clr.w d1                                                                   ; clear register d0
    move.b (sprite_count), d1                                                  ; sprint count in d0
    cmp.b #0, d1                                                               ; are there any sprites in the table?
    beq.s SkipSortSpriteTableByIndex                                           ; if not branch
    lsl.w #$03, d0                                                             ; calculate starting sprite address  in sprite table offset
                                                                               ; (sprite id * sprite size)
    lea (spritelist_table), a0                                                 ; sprite table in a0
    add.w d0, a0                                                               ; increment address by offset
    move.w (a0), (a1)                                                          ; copy address to calculate end address
    lsl.w #03, d1                                                              ; calculate sprite table offset by current sprite in sprite list
    add.w d1, (a1)                                                             ; last sprite in sprite table
    lea (a0), a2                                                               ; store copy of start address of sprite table in scratch for loop
    addi.w #8, (a2)                                                            ; temporary scratch for data to move
SortSpriteTableByIndexLoop:
    move.w (a2)+, (a0)+                                                        ; save sprites y position into new cell
    move.b (a2)+, (a0)+                                                        ; save sprites demensions into new cell in table
    move.b (a2)+, (a0)+                                                        ; index of next sprite 
    move.b (a2)+, (a0)+                                                        ; save sprites special bits
    move.b (a2)+, (a0)+                                                        ; save sprites tile id in table
    move.w (a2)+, (a0)+                                                        ; save sprites x position in table
    cmpa a0, a1                                                                ; did we sort all the sprites?
    bne.s SortSpriteTableByIndexLoop                                           ; if not branch
SkipSortSpriteTableByIndex:
    rts

; *************************************************************
; Sorts the sprite table by address 
; a0 - address in sprite table to start sorting from
; *************************************************************
SortSpriteTableByAddress:
    clr.w d1                                                                   ; clear register d0
    move.b (sprite_count), d1                                                  ; sprint count in d0
    cmp.b #0, d1                                                               ; are there any sprites in the table?
    beq.s SkipSortSpriteTableByAddress                                         ; if not branch
    move.w (a0), (a1)                                                          ; copy address to calculate end address
    lsl.w #03, d1                                                              ; calculate starting sprite address  in sprite table offset
                                                                               ; (sprite count * sprite size)
    add.w d1, (a1)                                                             ; address of last sprite in sprite table
    lea (a0), a2                                                               ; store copy of start address in sprite table in scratch for loop
    addi.w #8, (a2)                                                            ; temporary scratch for data to move
SortSpriteTableByAddressLoop:
    move.w (a2)+, (a0)+                                                        ; save sprites y position into new cell
    move.b (a2)+, (a0)+                                                        ; save sprites demensions into new cell in table
    move.b (a2)+, (a0)+                                                        ; index of next sprite 
    move.b (a2)+, (a0)+                                                        ; save sprites special bits
    move.b (a2)+, (a0)+                                                        ; save sprites tile id in table
    move.w (a2)+, (a0)+                                                        ; save sprites x position in table
    cmpa a0, a1                                                                ; did we sort all the sprites?
    bne.s SortSpriteTableByAddressLoop                                         ; if not branch
SkipSortSpriteTableByAddress:
    rts

; *************************************************************
; gets the sprite's x position from sprite table
; d0 - sprite id to get x position
; *************************************************************
getSpritePosXFromSpriteTable:
    lsl.w #$03, d0                                                             ; calculate sprite table offset (sprite count * sprite size)
    lea (spritelist_table), a0                                                 ; sprite table in a0
    add.w d0, a0                                                               ; increment address by offset
    addq #5, a0                                                                ; add 5 bytes to address (y position word+2, width/height byte+1,
                                                                               ; special byte+1, tile+1)
    move.w d0, (a0)                                                            ; store x position in d0
    rts

; *************************************************************
; sets the sprite's x position from sprite table
; d0 - sprite id to set x position
; d1 - sprites new x position
; *************************************************************
SetSpritePosXFromSpriteTable:
    lsl.w #$03, d0                                                             ; calculate sprite table offset (sprite count * sprite size)
    lea (spritelist_table), a0                                                 ; sprite table in a0
    add.w d0, a0                                                               ; increment address by offset
    addq #5, a0                                                                ; add 5 bytes to address (y position word+2, width/height byte+1,
                                                                               ; special byte+1, tile+1)
    move.w (a0), d1                                                            ; store new x position
    rts
; *************************************************************
; gets the sprite's y position from sprite table
; d0 - sprite id to get y position
; *************************************************************
getSpritePosYFromSpriteTable:
    lsl.w #$03, d0                                                             ; calculate sprite table offset (sprite count * sprite size)
    lea (spritelist_table), a0                                                 ; sprite table in a0
    add.w d0, a0                                                               ; increment address by offset
    move.w d0, (a0)                                                            ; store y position in d0
    rts

; *************************************************************
; sets the sprite's y position from sprite table
; d0 - sprite id to set y position
; d1 - sprites new y position
; *************************************************************
SetSpritePosYFromSpriteTable:
    lsl.w #$03, d0                                                             ; calculate sprite table offset (sprite count * sprite size)
    lea (spritelist_table), a0                                                 ; sprite table in a0
    add.w d0, a0                                                               ; increment address by offset
    move.w (a0), d1                                                            ; store new y position
    rts