;==============================================================
;   BIG EVIL CORPORATION .co.uk
;==============================================================
;   SEGA Genesis Framework (c) Matt Phillips 2014
;==============================================================
;   utility.asm - Miscellaneous utility subroutines
;==============================================================
ItoA_Hex_w:
   ; Converts a word to hex ASCII
   ; a0 --- In/Out: String address
   ; d0 (w) In: Number to convert

   ; 4 nybbles + 0x + terminator, working backwards
   add.l  #0x7, a0

   ; Zero terminate
   move.b #0x0, -(a0)

   move.w #0x0, d1   ; Char ptr
   move.w #0x3, d2   ; 4 nybbles in a word
@NybbleLp:
   move.b d0, d3         ; Byte to d3
   andi.b #0x0F, d3      ; Bottom nybble
   cmp.b  #0x9, d3
   ble.s  @Numeric         ; Branch if in numeric range
   add.b  #(ASCIIAlphaOffset-0xA), d3   ; In alpha range (A - F)
   move.b d3, -(a0)      ; Back to string
   lsr.w  #0x4, d0         ; Next nybble
   dbra   d2, @NybbleLp   ; Loop
   bra.s  @End
@Numeric:
   add.b  #ASCIINumericOffset, d3   ; In numeric range (0 - 9)
   move.b d3, -(a0)      ; Back to string
   lsr.w  #0x4, d0         ; Next nybble
   dbra   d2, @NybbleLp   ; Loop

   @End:

   ;0X
   move.b #'X', -(a0)
   move.b #'0', -(a0)

   rts

ItoA_Hex_l:
   ; Converts a long to hex ASCII
   ; a0 --- In/Out: String address
   ; d0 (w) In: Number to convert

   ; 8 nybbles + 0x + terminator, working backwards
   add.l  #0x0B, a0

   ; Zero terminate
   move.b #0x0, -(a0)

   move.w #0x0, d1   ; Char ptr
   move.w #0x7, d2   ; 8 nybbles in a long
@NybbleLp:
   move.b d0, d3         ; Byte to d3
   andi.b #0x0F, d3      ; Bottom nybble
   cmp.b  #0x9, d3
   ble.s  @Numeric         ; Branch if in numeric range
   add.b  #(ASCIIAlphaOffset-0xA), d3   ; In alpha range (A - F)
   move.b d3, -(a0)      ; Back to string
   lsr.w  #0x4, d0         ; Next nybble
   dbra   d2, @NybbleLp   ; Loop
   bra.s  @End
@Numeric:
   add.b  #ASCIINumericOffset, d3   ; In numeric range (0 - 9)
   move.b d3, -(a0)      ; Back to string
   lsr.w  #0x4, d0         ; Next nybble
   dbra   d2, @NybbleLp   ; Loop

   @End:

   ;0X
   move.b #'X', -(a0)
   move.b #'0', -(a0)

   rts

ItoA_Int_w:
   ; Converts a word to integer ASCII
   ; a0 --- In/Out: String address
   ; d0 (w) In: Number to convert

   ; 4 nybbles + terminator, working backwards
   add.l  #0x5, a0

   ; Zero terminate
   move.b #0x0, -(a0)

   move.w #0x0, d1       ; Char ptr
   move.w #0x3, d2       ; 4 nybbles in a word
@NybbleLp:
   move.b d0, d3         ; Byte to d3
   andi.b #0x0F, d3      ; Bottom nybble
   add.b #ASCIINumericOffset, d3   ; In numeric range (0 - 9)
   move.b d3, -(a0)      ; Back to string
   lsr.w #0x4, d0         ; Next nybble
   dbra d2, @NybbleLp   ; Loop

   rts

ItoA_Int_l:
   ; Converts a long to integer ASCII
   ; a0 --- In/Out: String address
   ; d0 (l) In: Number to convert

   ; 8 nybbles + terminator, working backwards
   add.l  #0x9, a0

   ; Zero terminate
   move.b #0x0, -(a0)

   move.w #0x0, d1       ; Char ptr
   move.w #0x7, d2       ; 8 nybbles in a word
@NybbleLp:
   move.b d0, d3         ; Byte to d3
   andi.b #0x0F, d3      ; Bottom nybble
   add.b #ASCIINumericOffset, d3   ; In numeric range (0 - 9)
   move.b d3, -(a0)      ; Back to string
   lsr.w #0x4, d0         ; Next nybble
   dbra d2, @NybbleLp   ; Loop

   rts

ItoBCD_Int_w:
   ; D0 (w) In: binary number (0-9999)
   ; D2 (w) Out: binary number converted to four digit BCD
   moveq   #0,d2       ;Clear conversion register.
   moveq   #3,d7       ;Number of BCD digits-1.
ItoBCDloop:
   divu    #10,d0      ;D0:LOW = D0/10, D0:HIGH = D0%10
   move.l  d0,d1       ;Copy to split quotient and remainder
   and.l   #$FFFF,d0   ;D0:HIGH = 0
   clr.w   d1          ;D1:LOW = 0, D1:HIGH = 0..9
   add.l   d1,d2       ;Add digit to binary number.
   ror.l   #4,d2       ;Pull it back into 16-bit window
   dbra    d7,ItoBCDloop
   rts