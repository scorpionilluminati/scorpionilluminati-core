;==============================================================
;   Scorpion Illuminati
;==============================================================
;   SEGA Genesis (c) SegaDev 2014
;==============================================================

; Sprite constants
sprite_size          equ 0x08
max_sprites          equ 0x50
sprite_plane_border  equ 0x0080

; Screen bounds
screen_width         equ 0x0140                                                ; 320 (H40)
screen_height        equ 0x00E0                                                ; 224 (V28)
screen_border_x      equ 0x0010
screen_border_y      equ 0x0010

; game state constants
game_state_title_screen_initalize       equ 0x0000
game_state_title_screen                 equ 0x0001
game_state_title_screen_shutdown        equ 0x0002
game_state_songlist_screen_initalize    equ 0x0003
game_state_songlist_screen              equ 0x0004
game_state_songlist_screen_shutdown     equ 0x0005
game_state_game_mode_initalize          equ 0x0006
game_state_game_mode                    equ 0x0007
game_state_game_mode_shutdown           equ 0x0008
game_state_game_mode_pause_initalize    equ 0x0009
game_state_game_mode_pause              equ 0x000A
game_state_game_mode_pause_shutdown     equ 0x000B
game_state_stats_screen_initalize       equ 0x000C
game_state_stats_screen                 equ 0x000D
game_state_stats_screen_shutdown        equ 0x000E

; selection arrow bounds
selectionarrow_boundry_top      equ 0x00A7
selectionarrow_boundry_bottom   equ 0x00C7

; note collision bounds
note_bounds_top      equ 0x0000
note_bounds_bottom   equ 0x0008
note_bounds_left     equ 0x0000
note_bounds_right    equ 0x0008

; object offbounds positions
offscreen_position_x:  equ 0x0000
offscreen_position_y:  equ 0x0000

; position for selection arrow
selectionarrow_start_position_y: equ 0x00A7
selectionarrow_start_position_x: equ 0x00AF
selectionarrow_y_velocity        equ 0x0002

; position for rock indicator
rockindicator_start_position_y:  equ 0x0150
rockindicator_start_position_x:  equ 0x00CF

; start position for notes
note_start_position_x:       equ 0x015E
green_note_start_position_y: equ 0x0118
red_note_start_position_y:   equ 0x0128

; arrow boundry locations
note_plane_border_offset:   equ 0x00C1
note_plane_safearea_offset: equ 0x00CD