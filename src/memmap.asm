;==============================================================
;     Scorpion Illuminati
;==============================================================
;   SEGA Genesis (c) SegaDev 2014
;==============================================================

; **********************************************
; Various size-ofs to make this easier/foolproof
; **********************************************
SizeByte:               equ 0x01
SizeWord:               equ 0x02
SizeLong:               equ 0x04
SizeSpriteDesc:         equ 0x08
SizeTile:               equ 0x20
SizePalette:            equ 0x40

    rsset 0
; ************************************
; Song structure
; ************************************
song_author_address         rs.l 1
song_title_address          rs.l 1
song_vgm_address            rs.l 1
song_beatmap_address        rs.l 1
song_struct_size            rs.b 0

; ************************************
; System stuff
; ************************************
    rsset 0x00FF0000
hblank_counter          rs.l 1                                         ; Start of RAM
vblank_counter          rs.l 1
audio_clock             rs.l 1
joypadA			rs.w 1
joypadA_press		rs.w 1
joypadB			rs.w 1
joypadB_press		rs.w 1

; ************************************
; Game globals
; ************************************
spritelist_table            rs.b sprite_size*max_sprites
sprite_count                rs.b 1
game_state                  rs.w 1
selectionarrow_position_y   rs.w 1
song_id                     rs.w 1
hits                        rs.w 1
misses                      rs.w 1
errors                      rs.w 1
score                       rs.w 1
combo                       rs.w 1
top_streak                  rs.w 1
completion                  rs.w 1
multiplier                  rs.w 1
scoredelta                  rs.w 1
rockindicator_position_x    rs.w 1
tempo                       rs.w 1
greennote_position_x        rs.w 1
rednote_position_x          rs.w 1

; ************************************
; crash handler storage
; ************************************
error_code                  rs.l 1
register_backup_a0          rs.l 1
register_backup_a1          rs.l 1
register_backup_a2          rs.l 1
register_backup_a3          rs.l 1
register_backup_a4          rs.l 1
register_backup_a5          rs.l 1
register_backup_a6          rs.l 1 
register_backup_d0          rs.l 1
register_backup_d1          rs.l 1
register_backup_d2          rs.l 1
register_backup_d3          rs.l 1
register_backup_d4          rs.l 1
register_backup_d5          rs.l 1
register_backup_d6          rs.l 1 
register_backup_d7          rs.l 1
backup_sp                   rs.l 1
backup_pc                   rs.l 1

__ramend                        rs.b 0