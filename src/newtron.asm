;==============================================================
;     Scorpion Illuminati
;==============================================================
;   SEGA Genesis (c) SegaDev 2014
;==============================================================
EnableDEBUG:	                  equ 0                                        ; change to 1 to enable debug code
EmulateDivideByZeroCrash:         equ 0                                        ; change to 1 to emulate a divide by zero crash
EmulateIllegalInstructionCrash:   equ 0                                        ; change to 1 to emulate a divide by zero crash