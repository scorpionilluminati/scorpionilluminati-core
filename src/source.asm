;==============================================================
;   Scorpion Illuminati
;==============================================================
;   SEGA Genesis (c) SegaDev 2014
;==============================================================

      ; Include SEGA Genesis ROM header and CPU vector table
      include 'header.asm'

      ; assembler directives
      include 'newtron.asm'

      ; include framework code
      include 'framewk\macros.asm'
      include 'framewk\init.asm'
      include 'framewk\collisn.asm'
      if EnableDebug=1
      include 'framewk\debugger.asm'                                         ; NOT FOR RELEASE
      endc
      include 'framewk\gamepad.asm'
      include 'framewk\interpts.asm'
      include 'framewk\megacd.asm'
      include 'framewk\memory.asm'
      include 'framewk\psg.asm'
      include 'framewk\sprites.asm'
      include 'framewk\sprlist.asm'
      include 'framewk\text.asm'
      include 'framewk\timing.asm'
      include 'framewk\tmss.asm'
      include 'framewk\palettes.asm'
      include 'framewk\planes.asm'
      include 'framewk\utility.asm'
      include 'framewk\vdp.asm'
      include 'framewk\z80.asm'
      include 'framewk\echo.68k'
      include 'assets\states\title\init.asm'
      include 'assets\states\title\main.asm'
      include 'assets\states\title\shutdown.asm'
      include 'assets\states\songlist\init.asm'
      include 'assets\states\songlist\main.asm'
      include 'assets\states\songlist\shutdown.asm'
      include 'assets\states\game\init.asm'
      include 'assets\states\game\main.asm'
      include 'assets\states\game\shutdown.asm'
      include 'assets\states\pause\init.asm'
      include 'assets\states\pause\main.asm'
      include 'assets\states\pause\shutdown.asm'
      include 'assets\states\stats\init.asm'
      include 'assets\states\stats\main.asm'
      include 'assets\states\stats\shutdown.asm'
      include 'assets\states\crashand\init.asm'
      include 'assets\states\crashand\main.asm'
      include 'assets\states\crashand\shutdown.asm'
      include 'utility.asm'

__main:

      ; ************************************
      ; Load palettes
      ; ************************************
	  lea Palette, a0                                                        ; Move Palette address to a0
	  moveq #0x0, d0                                                         ; Palette ID in d0
	  jsr LoadPalette                                                        ; Jump to subroutine

      ; ************************************
      ;  Load the Pixel Font
      ; ************************************
      lea PixelFont, a0                                                        ; Move font address to a0
      move.l #PixelFontVRAM, d0                                                ; Move VRAM dest address to d0
      move.l #PixelFontSizeT, d1                                               ; Move number of characters (font size in tiles) to d1
      jsr LoadFont                                                             ; Jump to subroutine

      ; *************************************
      ; Load the selection arrow sprite
      ; *************************************
      lea SelectionArrow, a0                                                   ; Move sprite address to a0
      move.l #SelectionArrowVRAM, d0                                           ; Move VRAM dest address to d0
      move.l #SelectionArrowSizeT, d1                                          ; Move number of tiles to d1
      jsr LoadTiles                                                            ; Jump to subroutine

      ; *************************************
      ; Load the green note sprite
      ; *************************************
      lea GreenNote, a0                                                        ; Move sprite address to a0
      move.l #GreenNoteVRAM, d0                                                ; Move VRAM dest address to d0
      move.l #GreenNoteSizeT, d1                                               ; Move number of tiles to d1
      jsr LoadTiles                                                            ; Jump to subroutine

      ; *************************************
      ; Load the red note sprite
      ; *************************************
      lea RedNote, a0                                                          ; Move sprite address to a0
      move.l #RedNoteVRAM, d0                                                  ; Move VRAM dest address to d0
      move.l #RedNoteSizeT, d1                                                 ; Move number of tiles to d1
      jsr LoadTiles                                                            ; Jump to subroutine

      ; *************************************
      ; Load the rock indicator sprite
      ; *************************************
      lea RockIndicator, a0                                                    ; Move sprite address to a0
      move.l #RockIndicatorVRAM, d0                                            ; Move VRAM dest address to d0
      move.l #RockIndicatorSizeT, d1                                           ; Move number of tiles to d1
      jsr LoadTiles                                                            ; Jump to subroutine

      ; ************************************
      ; Load sprite descriptors
      ; ************************************
      lea sprite_descriptor_table, a0                                          ; Sprite table data
      move.w #number_of_sprites, d0                                            ; 5 sprites
      jsr LoadSpriteTables

      ; ************************************
      ; Load title screen map tiles
      ; ************************************
      lea TitleScreenTiles, a0                                                 ; Move sprite address to a0
      move.l #TitleScreenTilesVRAM, d0                                         ; Move VRAM dest address to d0
      move.l #TitleScreenTilesSizeT, d1                                        ; Move number of tiles to d1
      jsr LoadTiles                                                            ; Jump to subroutine

      ; ************************************
      ; Load songlist screen map tiles
      ; ************************************
      lea SonglistScreenTiles, a0                                                 ; Move sprite address to a0
      move.l #SonglistScreenTilesVRAM, d0                                         ; Move VRAM dest address to d0
      move.l #SonglistScreenTilesSizeT, d1                                        ; Move number of tiles to d1
      jsr LoadTiles                                                               ; Jump to subroutine

      ; ************************************
      ; Load game map tiles
      ; ************************************
      lea GameTiles, a0                                                        ; Move sprite address to a0
      move.l #GameTilesVRAM, d0                                                ; Move VRAM dest address to d0
      move.l #GameTilesSizeT, d1                                               ; Move number of tiles to d1
      jsr LoadTiles                                                            ; Jump to subroutine

      ; ************************************
      ; Load stats screen map tiles
      ; ************************************
      lea StatsScreenTiles, a0                                                 ; Move sprite address to a0
      move.l #StatsScreenTilesVRAM, d0                                         ; Move VRAM dest address to d0
      move.l #StatsScreenTilesSizeT, d1                                        ; Move number of tiles to d1
      jsr LoadTiles                                                            ; Jump to subroutine

      ; *************************************
      ; Init Echo driver
      ; *************************************
      lea (InstrumentList), a0                                                 ; list of instruments to load
      bsr Echo_Init

      ; ************************************
      ; One Time Inits
      ; ************************************
      move.w #game_state_title_screen_initalize, (game_state)                  ; set the game state

      ; ******************************************************************
      ; Main game loop
      ; ******************************************************************
GameLoop:

      moveq   #0,d1                                                             ; clear d1
      move.w  game_state,d1                                                     ; get game state on d1
      add.w   d1,d1                                                             ; multiply ID by 2 (if d1 = 1 -> d1 = 2)
      add.w   d1,d1                                                             ; multiply previous by 2 (if d1 = 2 -> d1 = 4)
      jsr     @index(pc,d1.w)                                                   ; jump to @index addr + d1
      bra     GameLoop

@index:
      bra.w   TitleScreen_Init                                                 ; (0)
      bra.w   TitleScreen                                                      ; (1)
      bra.w   TitleScreen_Shutdown                                             ; (2)
      bra.w   SonglistScreen_Init                                              ; (3)
      bra.w   SonglistScreen                                                   ; (4)
      bra.w   SonglistScreen_Shutdown                                          ; (5)
      bra.w   GameMode_Init                                                    ; (6)
      bra.w   GameMode                                                         ; (7)
      bra.w   GameMode_Shutdown                                                ; (8)
      bra.w   Pause_Init                                                       ; (9)
      bra.w   Pause                                                            ; (A)
      bra.w   Pause_Shutdown                                                   ; (B)
      bra.w   StatsScreen_Init                                                 ; (C)
      bra.w   StatsScreen                                                      ; (D)
      bra.w   StatsScreen_Shutdown                                             ; (E)

      jsr WaitVBlankEnd                                                        ; Wait for end of vblank

      jmp GameLoop                                                             ; Back to the top

      ; ************************************
      ; Data
      ; ************************************

      ; Include framework data
      include 'framewk\initdata.asm'
      include 'framewk\globals.asm'
      include 'framewk\charmap.asm'

      ; Include game data
      include 'globals.asm'
      include 'memmap.asm'

      ; Include game art
      include 'assets\assetmap.asm'
	include 'sprdespt.asm'

__end                                                                          ; Very last line, end of ROM address

      inform 0, "*********************"                                        ; start of binary statistics header
      inform 0, "* Binary Statistics *"                                        ; binary statistics header
      inform 0, "*********************"                                        ; end of binary statistics header
      inform 0, ""                                                             ; spacer
      inform 0, "*********************"                                        ; start of rom usage header
      inform 0, "*     Rom Usage     *"                                        ; rom usage header
      inform 0, "*********************"                                        ; end of rom usage header
      inform 0,"%d bytes used", __end-$200                                     ; number of bytes used in cartridge space
      inform 0,"%d bytes left", $400000-__end                                  ; number of bytes remaining in cartridge space
      inform 0, ""                                                             ; spacer
      inform 0, "*********************"                                        ; start of ram usage header
      inform 0, "*     Ram Usage     *"                                        ; ram usage header
      inform 0, "*********************"                                        ; end of ram usage header
      inform 0,"%d bytes of ram used", __ramend-$FF0000                        ; number of bytes used in ram space
      inform 0,"%d bytes of ram left", $FFFFFF-__ramend                        ; number of bytes remaining in ram space