;==============================================================
;   Scorpion Illuminati
;==============================================================
;   SEGA Genesis (c) SegaDev 2014
;==============================================================

; Sprite IDs
selectionarrow_id equ 0x0
greennote_id      equ 0x1
rednote_id        equ 0x2
yellownote_id     equ 0x3
bluenote_id       equ 0x4
orangenote_id     equ 0x5
rockindicator_id  equ 0x6
number_of_sprites equ 0x7

    ; Sprite descriptor structs
sprite_descriptor_table:
selectionarrow_sprite:
      dc.w offscreen_position_y                                                ; Y coord (+ 128)
      dc.b SelectionArrowDimensionBits                                         ; Width (bits 0-1) and height (bits 2-3) in tiles
      dc.b 0x01                                                                ; Index of next sprite (linked list)
      dc.b 0x00                                                                ; H/V flipping (bits 3/4), palette index (bits 5-6), priority (bit 7)
      dc.b SelectionArrowTileID                                                ; Index of first tile
      dc.w offscreen_position_x                                                ; X coord (+ 128)
greennote_sprite:
      dc.w offscreen_position_y                                                ; Y coord (+ 128)
      dc.b GreenNoteDimensionBits                                              ; Width (bits 0-1) and height (bits 2-3) in tiles
      dc.b 0x02                                                                ; Index of next sprite (linked list)
      dc.b 0x00                                                                ; H/V flipping (bits 3/4), palette index (bits 5-6), priority (bit 7)
      dc.b GreenNoteTileID                                                     ; Index of first tile
      dc.w offscreen_position_x                                                ; X coord (+ 128)
rednote_sprite:
      dc.w offscreen_position_y                                                ; Y coord (+ 128)
      dc.b RedNoteDimensionBits                                                ; Width (bits 0-1) and height (bits 2-3) in tiles
      dc.b 0x03                                                                ; Index of next sprite (linked list)
      dc.b 0x00                                                                ; H/V flipping (bits 3/4), palette index (bits 5-6), priority (bit 7)
      dc.b RedNoteTileID                                                       ; Index of first tile
      dc.w offscreen_position_x                                                ; X coord (+ 128)
rockindicator_sprite:
      dc.w offscreen_position_y                                                ; Y coord (+ 128)
      dc.b RockIndicatorDimensionBits                                          ; Width (bits 0-1) and height (bits 2-3) in tiles
      dc.b 0x04                                                                ; Index of next sprite (linked list)
      dc.b 0x00                                                                ; H/V flipping (bits 3/4), palette index (bits 5-6), priority (bit 7)
      dc.b RockIndicatorTileID                                                 ; Index of first tile
      dc.w offscreen_position_x                                                ; X coord (+ 128)