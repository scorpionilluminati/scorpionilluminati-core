; ==============================================================
; GetSongData
; d3 - index of song data to load
; ==============================================================
GetSongData:
      ; Get song address in table
      mulu #song_struct_size, d3                                               ; Index to offset
      lea SongList, a3                                                         ; Get song table address
      add.l	d3, a3                                                             ; Add offset
      move.l song_vgm_address(a3), a4                                          ; Get vgm ptr, to a4
      move.l song_beatmap_address(a3), a5                                      ; Get beatmap ptr, to a5
      rts
; ==============================================================
; GetSongMetaData
; d3 - index of song data to load
; ==============================================================
GetSongMetaData:
      ; Get song address in table
      mulu #song_struct_size, d3                                               ; Index to offset
      lea SongList, a3                                                         ; Get song table address
      add.l	d3, a3                                                             ; Add offset
      move.l song_author_address(a3), a4                                       ; Get author name ptr, to a4
      move.l song_title_address(a3), a5                                        ; Get song title ptr, to a5
      rts