@echo off
rem ************************************
rem path to your emulator
rem ************************************
@echo on
SET emupath=c:\blastem\blastem.exe

@echo off
rem ************************************
rem any flags required by your emulator
rem ************************************
@echo on
SET flags=-d

@echo off
rem ************************************
rem path to your homebrew rom
rem ************************************
@echo on
SET rompath=c:\segadev\bin\emu\rom.bin

@echo off
rem ************************************
rem DO NOT EDIT ANYTHING BELOW THIS LINE
rem ************************************
@echo on

%emupath% %flags% %rompath%
pause